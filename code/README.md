# README #

Projeto de Graduação de Bruno Manzoli do Nascimento: _SAE – Sistema de Acompanhamento de Egressos_.

### Resumo ###

O Departamento de Informática da Universidade Federal do Espírito Santo (DI/Ufes) necessita de um sistema de informação para o acompanhamento dos alunos egressos, com o propósito estimular o interesse principalmente alunos do ensino médio pela área da informática. Para isso, os egressos forneceriam dados como área de atuação, faixa salarial, curso de pós-graduação realizados, possibilitando assim obter informações de perfil dos egressos e gerar relatórios estatísticos que ficarão disponíveis na Internet.

Para a construção do sistema, seguiu-se um processo de Engenharia de Software realizando as etapas de levantamento de requisitos, especificação de requisitos, definição da arquitetura do sistema, implementação e testes. Foram colocadas em prática as disciplinas aprendidas no decorrer do curso, tais como Engenharia de Software, Engenharia de Requisitos, Projeto de Sistema de Software, Programação Orientada a Objetos e Desenvolvimento Web e Web Semântica. Também foram utilizados métodos e técnicas de modelagem e desenvolvimento orientado a objetos, em particular o método FrameWeb para projeto de aplicações Web baseadas em frameworks.

