package br.ufes.inf.nemo.marvin.sae.core.control;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;

import br.ufes.inf.nemo.marvin.sae.core.application.ManageTopicsOfInterestSevice;
import br.ufes.inf.nemo.marvin.sae.core.domain.TopicOfInterest;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.controller.CrudController;



/**
 * Controller class responsible for mediating the communication between user interface and application service for the
 * use case "Manage Assunto_Interesse".
 * 
 * This use case is a CRUD and, thus, the controller also uses the mini CRUD framework for EJB3.
 * 
 * @author Bruno Manzoli (manzoli2122@gmail.com)
 */
@Named
@SessionScoped
public class ManageTopicsOfInterestControl extends CrudController<TopicOfInterest>{

	
	/** Serialization id. */
	private static final long serialVersionUID = 1L;

	
	/** The logger. */
	private static final Logger logger = Logger.getLogger(ManageTopicsOfInterestControl.class.getCanonicalName());
	
	
	/** The "Manage Assunto_Interesse" service. */
	@EJB
	private ManageTopicsOfInterestSevice manageTopicsOfInterestService;
	
	
	
	
	
	
	
	/**   CONSTRUTOR DA CLASSE  */
	public ManageTopicsOfInterestControl(){
		 viewPath = "/sae/core/manageTopicsOfInterest/";
	     bundleName = "msgsSAE";
	}
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#getCrudService() */
	@Override
	protected CrudService<TopicOfInterest> getCrudService() {
		manageTopicsOfInterestService.authorize();
		return manageTopicsOfInterestService;
	}

	
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#initFilters() */
	@Override
	protected void initFilters() {
	}
	
	
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#save() */
	@Override
	public String save() {
		try{
			return super.save();
		}
		catch(Exception e){
			selectedEntity.setId(null);
			addGlobalI18nMessage(getBundleName(), FacesMessage.SEVERITY_ERROR, getBundlePrefix() + ".error.save" , summarizeSelectedEntity()  );
			return null;
		}
	}
	
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#delete() */
	@Override
	public String delete() {
		try{
			return super.delete();
		}
		catch(Exception e){
			addGlobalI18nMessage(getBundleName(), FacesMessage.SEVERITY_ERROR, getBundlePrefix() + ".error.delete", summarizeSelectedEntity());
			cancelDeletion();
	        return null;
		}
	}
	
	
	
}
