package br.ufes.inf.nemo.marvin.sae.core.domain;

import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport;
import br.ufes.inf.nemo.marvin.core.domain.Academic;

/**
 * CLASSE DE DOMMINIO QUE REPRESENTA O ASSUNTOS RELACIONADOS AO DEPARTAMENTO DE INFORMÁTICA.
 * 
 * IMPLEMENTADA COM BASE NO Documento de Análise de Requisitos VERSÃO 1.4
 * 
 * <i>ESTA CLASSE FAZ PARTE DO SISTEMA SAE.</i>
 * 
 * @author BRUNO MANZOLI (manzoli2122@gmail.com)
 */
@Entity
public class TopicOfInterest extends PersistentObjectSupport implements Comparable<TopicOfInterest> {

	
	private static final long serialVersionUID = 1L;
	
	
	/** name DO ASSUNTO DE INTERESSE */
	@NotNull
	@Size(max = 60)
	@Column(unique=true)
	private String name;
	
	
	@ManyToMany(fetch=FetchType.LAZY)
	private Set<Academic> academics;

	
	


	@Override
	public int compareTo(TopicOfInterest o) { 
		if (name == null)	return 1;
		if (o.name == null) return -1;
		
		int cmp = name.compareTo(o.name);
		if (cmp != 0) return cmp;
		
		return super.compareTo(o); 
	}
	
	
	@Override
	public String toString() { return name; }

	
	
	
	
	/**  GETS AND SETS  */
	public String getName() {return name; }
	public void setName(String name) { 	this.name = name; }
	
	public Set<Academic> getAcademics() { 	return academics; 	}
	public void setAcademics(Set<Academic> academics) { this.academics = academics; }
	
}
