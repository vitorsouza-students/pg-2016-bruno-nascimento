package br.ufes.inf.nemo.marvin.sae.core.control;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import br.ufes.inf.nemo.marvin.sae.core.application.ManageSeminarsService;
import br.ufes.inf.nemo.marvin.sae.core.domain.Seminar;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.controller.CrudController;


@Named
@SessionScoped
public class ManageSeminarsControl extends CrudController<Seminar>{

private static final long serialVersionUID = 1L;
	
	
	@EJB
	private ManageSeminarsService manageSeminarService;

	/*   CONSTRUTOR DA CLASSE */
	public ManageSeminarsControl(){
		 viewPath = "/sae/core/manageSeminars/";
	     bundleName = "msgsSAE";
	}

	
	@Override
	protected CrudService<Seminar> getCrudService() {
		manageSeminarService.authorize();
		return manageSeminarService;
	}


	@Override
	protected void initFilters() {
		
	}
	
	public String confirmar(){
		manageSeminarService.confirmSeminar(selectedEntity);
		return list();
	}
	
	
}
