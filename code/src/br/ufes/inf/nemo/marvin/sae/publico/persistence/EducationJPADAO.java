package br.ufes.inf.nemo.marvin.sae.publico.persistence;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;

import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Education;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Education_;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseJPADAO;


@Stateless
public class EducationJPADAO extends BaseJPADAO<Education> implements EducationDAO{
	private static final long serialVersionUID = 1L;

	@PersistenceContext(unitName="Marvin")
	private EntityManager entityManager;

	

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}

	
	@Override
	protected List<Order> getOrderList(CriteriaBuilder cb, Root<Education> root) {
		List<Order> orderList = new ArrayList<Order>();
		orderList.add(cb.asc(root.get(Education_.degree)));
		return orderList;
	}

	@Override
	public List<Education> retrieveAllMine(Academic academic) {
		EntityManager em = getEntityManager();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Education> cq = cb.createQuery(getDomainClass());
		Root<Education> root = cq.from(getDomainClass());
		
		cq.where(  cb.equal(root.get(Education_.academic), academic));
		
		cq.select(root);

		// Applies ordering.
		applyOrdering(cb, root, cq);

		// Return the list of objects.
		List<Education> result = em.createQuery(cq).getResultList();
		return result;
	}

	
}
