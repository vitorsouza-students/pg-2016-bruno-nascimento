package br.ufes.inf.nemo.marvin.sae.core.control;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import br.ufes.inf.nemo.marvin.sae.core.application.ManageCoursesRealizedService;
import br.ufes.inf.nemo.marvin.sae.core.domain.CourseRealized;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.controller.CrudController;


/**
 * Controller class responsible for mediating the communication between user interface and application service for the
 * use case "Manage CursoRealizado".
 * 
 * This use case is a CRUD and, thus, the controller also uses the mini CRUD framework for EJB3.
 * 
 * @author Bruno Manzoli (manzoli2122@gmail.com)
 */
@Named
@SessionScoped
public class ManageCoursesRealizedControl extends CrudController<CourseRealized> {

		
	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	
	/** The logger. */
	private static final Logger logger = Logger.getLogger(ManageCoursesRealizedControl.class.getCanonicalName());
	
	
	/** The "Manage CursoRealizado" service. */
	@EJB
	private ManageCoursesRealizedService manageCoursesRealizedService;

	
	/** The controller de Egresso for get selected egresso*/ 
	//@Inject
	//private ManageEgressoControl manageEgressoControl;
	
	
	
	
	
	
	
	/*   CONSTRUTOR DA CLASSE */
	public ManageCoursesRealizedControl(){
		 viewPath = "/sae/core/manageCoursesRealized/";
	     bundleName = "msgsSAE";
	}
	
	
	
	public List<CourseRealized> getMyCourseRealized() {
		return manageCoursesRealizedService.getMyCourseRealized();
	}
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#getCrudService() */
	@Override
	protected CrudService<CourseRealized> getCrudService() {
		manageCoursesRealizedService.authorize();
		return manageCoursesRealizedService;
	}

	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#initFilters() */
	@Override
	protected void initFilters() {
	}
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#create() */
	@Override
	public String create() {
		readOnly = false;
		selectedEntity = createNewEntity();
	//	if (manageEgressoControl.getSelectedEntity()!=null){
	//		selectedEntity.setEgresso(manageEgressoControl.getSelectedEntity());
	//	}
		return getViewPath() + "form.xhtml?faces-redirect=" + getFacesRedirect();
	}

	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#save() */
	@Override
	public String save() {
		try{
			return super.save();
		}
		catch(Exception e){
			selectedEntity.setId(null);
			addGlobalI18nMessage(getBundleName(), FacesMessage.SEVERITY_ERROR, getBundlePrefix() + ".error.save" , summarizeSelectedEntity()  );
			return null;
		}
	}
	
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#delete() */
	@Override
	public String delete() {
		try{
			return super.delete();
		}
		catch(Exception e){
			addGlobalI18nMessage(getBundleName(), FacesMessage.SEVERITY_ERROR, getBundlePrefix() + ".error.delete", summarizeSelectedEntity());
			cancelDeletion();
	        return null;
		}
	}
	
	
	
}
