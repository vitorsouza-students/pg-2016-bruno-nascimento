package br.ufes.inf.nemo.marvin.sae.core.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.marvin.sae.core.domain.CourseRealized;
import br.ufes.inf.nemo.marvin.sae.core.domain.CourseRealized_;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseJPADAO;


@Stateless
public class CourseRealizedJPADAO extends BaseJPADAO<CourseRealized> implements CourseRealizedDAO{

	private static final long serialVersionUID = 1L;

	@PersistenceContext(unitName="Marvin")
	private EntityManager entityManager;
	
	

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	
	@Override
	protected List<Order> getOrderList(CriteriaBuilder cb, Root<CourseRealized> root) {
		List<Order> orderList = new ArrayList<Order>();
		orderList.add(cb.asc(root.get(CourseRealized_.course.getName())));
		return orderList;
	}
	
	
	@Override
	public List<CourseRealized> retrieveMyCourses(Academic academic){
		
		EntityManager em = getEntityManager();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<CourseRealized> cq = cb.createQuery(getDomainClass());
		Root<CourseRealized> root = cq.from(getDomainClass());		
		cq.where(cb.equal(root.get(CourseRealized_.alumni),academic));
		cq.select(root);
		applyOrdering(cb, root, cq);
		List<CourseRealized> result = em.createQuery(cq).getResultList();
		return result;
	}
	
	
	@Override
	public List<CourseRealized> retrieveAllCourse(Course course) {
		
		if(course==null) return null;
	
		EntityManager em = getEntityManager();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<CourseRealized> cq = cb.createQuery(getDomainClass());
		Root<CourseRealized> root = cq.from(getDomainClass());
		
		cq.where(
				cb.equal(root.get(CourseRealized_.course),course)							
		  );
		
		cq.select(root);

		// Applies ordering.
		List<Order> orderList = new ArrayList<Order>();
		orderList.add(cb.desc(root.get(CourseRealized_.endYear)));
		if (orderList != null) cq.orderBy(orderList);

		// Return the list of objects.
		List<CourseRealized> result = em.createQuery(cq).getResultList();
		return result;
		//return super.retrieveAll();
	}
	

}
