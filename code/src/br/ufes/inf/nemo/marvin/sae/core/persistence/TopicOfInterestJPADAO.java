package br.ufes.inf.nemo.marvin.sae.core.persistence;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.ufes.inf.nemo.marvin.sae.core.domain.TopicOfInterest;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseJPADAO;



@Stateless
public class TopicOfInterestJPADAO extends BaseJPADAO<TopicOfInterest> implements TopicOfInterestDAO{

	
	private static final long serialVersionUID = 1L;
	
	@PersistenceContext(unitName="Marvin")
	private EntityManager entityManager;


	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}

}
