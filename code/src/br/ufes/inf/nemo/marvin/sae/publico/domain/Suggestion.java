package br.ufes.inf.nemo.marvin.sae.publico.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import br.ufes.inf.nemo.marvin.sae.core.domain.CourseRealized;
import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport;

@Entity
public class Suggestion extends PersistentObjectSupport implements Comparable<Suggestion>{

	
	private static final long serialVersionUID = 1L;
	
	
	/* NOME DO USUARIO */
	@NotNull
	@Lob
	private String content;
	
	/* NOME DO USUARIO */
	@Lob
	private String answer;	
	
	/* NOME DO USUARIO */
	@NotNull
	private Date send_date;
	
	/* NOME DO USUARIO */
	@NotNull
	@ManyToOne
	private CourseRealized courseRealized;
	
	
	
	
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Date getSend_date() {
		return send_date;
	}

	public void setSend_date(Date send_date) {
		this.send_date = send_date;
	}

	public CourseRealized getCourseRealized() {
		return courseRealized;
	}

	public void setCourseRealized(CourseRealized cursoRealized) {
		this.courseRealized = cursoRealized;
	}

	@Override
	public int compareTo(Suggestion  o) { return super.compareTo(o); }
	
	@Override
	public String toString() { return courseRealized.getCourse().getName() + " na data " +send_date.toString(); }
}
