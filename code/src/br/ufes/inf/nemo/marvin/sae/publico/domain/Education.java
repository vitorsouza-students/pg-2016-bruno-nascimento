package br.ufes.inf.nemo.marvin.sae.publico.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport;


/**
 * CLASSE DE DOMMINIO QUE REPRESENTA A ESCOLARIDADE DO EGRESSO.
 * 
 * IMPLEMENTADA COM BASE NO Documento de Análise de Requisitos VERSÃO 1.4
 * 
 * <i>ESTA CLASSE FAZ PARTE DO SISTEMA SAE.</i>
 * 
 * @author BRUNO MANZOLI (manzoli2122@gmail.com)
 */

@Entity
public class Education extends PersistentObjectSupport implements Comparable<Education>{

	
	private static final long serialVersionUID = 1L;
	
	
	/** EGRESSO DA ESCOLARIDADE */
	@NotNull
	@ManyToOne
	private Academic academic;
	
	
	/** TITULO DO CURSO */
	@NotNull
	private Degree_Education degree;
	
	
	/** ESTADO ONDE FOI REALIZADO O CURSO */
	@NotNull
	@Size(max = 60)
	private String state;
	
	
	/** PAIS ONDE FOI REALIZADO O CURSO */
	@NotNull
	@Size(max = 60)
	private String country;
	
	
	/** INSTITUICAO ONDE FOI REALIZADO O CURSO*/
	@NotNull
	@Size(max = 60)
	private String institution;
	
	/** ANO DE TERMINO DO CURSO */
	@NotNull
	private int year;
	
	
	
	
	
	
	@Override
	public int compareTo(Education  o) { 
		
		if (academic == null)	return 1;
		if (o.academic == null) return -1;
		int cmp = academic.compareTo(o.academic);
		if (cmp != 0 ) return cmp;
				
		
		if (degree == null)	return 1;
		if (o.degree == null) return -1;		
		cmp = degree.compareTo(o.degree);
		if (cmp != 0 ) return cmp;
				
		return super.compareTo(o);
		
	}
	
	@Override
	public String toString() { return academic.toString(); }

	
	

	
	
	
	
	/**  GETS AND SETS  */
	public Academic getAcademic() {
		return academic;
	}

	public void setAcademic(Academic academic) {
		this.academic = academic;
	}

	public Degree_Education getDegree() {
		return degree;
	}

	public void setDegree(Degree_Education degree) {
		this.degree = degree;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	
	
}

