package br.ufes.inf.nemo.marvin.sae.publico.domain;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport_;
import br.ufes.inf.nemo.marvin.sae.core.domain.CourseRealized;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-08T11:59:23.013-0300")
@StaticMetamodel(Suggestion.class)
public class Suggestion_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<Suggestion, String> content;
	public static volatile SingularAttribute<Suggestion, String> answer;
	public static volatile SingularAttribute<Suggestion, Date> send_date;
	public static volatile SingularAttribute<Suggestion, CourseRealized> courseRealized;
}
