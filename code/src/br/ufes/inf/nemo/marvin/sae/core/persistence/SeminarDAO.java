package br.ufes.inf.nemo.marvin.sae.core.persistence;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.sae.core.domain.Seminar;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;

@Local
public interface SeminarDAO extends BaseDAO<Seminar>{

}
