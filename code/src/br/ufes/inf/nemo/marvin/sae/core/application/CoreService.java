package br.ufes.inf.nemo.marvin.sae.core.application;

import java.io.Serializable;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.sae.core.domain.SaeConfiguracao;



/**
 * Singleton bean that stores in memory information that is useful for the entire application, i.e., read-only
 * information shared by all users. This bean stores information for the core package.
 * 
 * @author Bruno Manzoli (manzoli2122@gmail.com)
 */
@Local
public interface CoreService extends Serializable {

	
	/** Getter for SaeConfiguracao. */
	public SaeConfiguracao getCurrentConfig() ;

	public  void sendEmailUpdate();
	
}
