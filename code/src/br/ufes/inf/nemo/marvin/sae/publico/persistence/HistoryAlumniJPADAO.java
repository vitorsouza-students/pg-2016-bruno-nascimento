package br.ufes.inf.nemo.marvin.sae.publico.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.marvin.sae.core.domain.CourseRealized;
import br.ufes.inf.nemo.marvin.sae.core.domain.CourseRealized_;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Salary_Level;
import br.ufes.inf.nemo.marvin.sae.publico.domain.HistoryAlumni;
import br.ufes.inf.nemo.marvin.sae.publico.domain.HistoryAlumni_;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseJPADAO;

@Stateless
public class HistoryAlumniJPADAO  extends BaseJPADAO<HistoryAlumni> implements HistoryAlumniDAO{

	private static final long serialVersionUID = 1L;

	@PersistenceContext(unitName="Marvin")
	private EntityManager entityManager;
	
	
	
	
	
	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}

	
	
	@Override
	protected List<Order> getOrderList(CriteriaBuilder cb, Root<HistoryAlumni> root) {
		List<Order> orderList = new ArrayList<Order>();
		orderList.add(cb.asc(root.get(HistoryAlumni_.send_date)));
		return orderList;
	}
	
	
	
	@Override
	public long contFaixaSalarial(Salary_Level faixa, Course course){
		/*
		
		EntityManager em = getEntityManager();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		
		
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<HistoryAlumni> root = cq.from(getDomainClass());
		
		// SUBQUERYH
		Subquery<HistoryAlumni> subqueryH = cq.subquery(HistoryAlumni.class);
		Root<HistoryAlumni> subrootH = subqueryH.from(HistoryAlumni.class);
		subqueryH.where(
							cb.equal(root.get(HistoryAlumni_.egresso),subrootH.get(HistoryAlumni_.egresso)),
							cb.lessThan(root.get(HistoryAlumni_.data_envio), subrootH.get(HistoryAlumni_.data_envio))
							
					  );
		subqueryH.select(subrootH);
		
		// SUBQUERY
		Subquery<CourseRealize> subquery = cq.subquery(CourseRealize.class);
		Root<CourseRealize> subroot = subquery.from(CourseRealize.class);
		subquery.where(
							cb.equal(root.get(HistoryAlumni_.egresso),subroot.get(CourseRealize_.egresso)),
							cb.equal(subroot.get(CourseRealize_.course), course)
					  );
		subquery.select(subroot);
		cq.where(
					cb.equal(root.get(HistoryAlumni_.faixa_salarial),faixa ),
					cb.exists(  subquery  ),
					cb.not(cb.exists(subqueryH))
				);
		
		
		cq.select(cb.count(root));
		Query q = em.createQuery(cq);

		// Retrieve the value and return.
		long count = ((Long) q.getSingleResult()).longValue();
		return count;
		*/
	
		return 1L;
	}
	
	
	
	
	@Override
	public long contReside(Boolean mora, Course course){
		/*
		
		EntityManager em = getEntityManager();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		
		
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<HistoryAlumni> root = cq.from(getDomainClass());
		
		// SUBQUERYH
		Subquery<HistoryAlumni> subqueryH = cq.subquery(HistoryAlumni.class);
		Root<HistoryAlumni> subrootH = subqueryH.from(HistoryAlumni.class);
		subqueryH.where(
							cb.equal(root.get(HistoryAlumni_.egresso),subrootH.get(HistoryAlumni_.egresso)),
							cb.lessThan(root.get(HistoryAlumni_.data_envio), subrootH.get(HistoryAlumni_.data_envio))
							
					  );
		subqueryH.select(subrootH);
		
		// SUBQUERY
		Subquery<CourseRealize> subquery = cq.subquery(CourseRealize.class);
		Root<CourseRealize> subroot = subquery.from(CourseRealize.class);
		subquery.where(
							cb.equal(root.get(HistoryAlumni_.egresso),subroot.get(CourseRealize_.egresso)),
							cb.equal(subroot.get(CourseRealize_.course), course)
					  );
		subquery.select(subroot);
		cq.where(
					cb.equal(root.get(HistoryAlumni_.reside_no_ES),mora ),
					cb.exists(  subquery  ),
					cb.not(cb.exists(subqueryH))
				);
		
		
		cq.select(cb.count(root));
		Query q = em.createQuery(cq);

		// Retrieve the value and return.
		long count = ((Long) q.getSingleResult()).longValue();
		
		return count;   */
	
		return 1L;
		
	}

	
	
	@Override
	public List<HistoryAlumni> searchHistories(Course course) {
		
		EntityManager em = getEntityManager();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		
		
		CriteriaQuery<HistoryAlumni> cq = cb.createQuery(HistoryAlumni.class);
		Root<HistoryAlumni> root = cq.from(getDomainClass());
		
		
		// SUBQUERYH
		Subquery<HistoryAlumni> subqueryH = cq.subquery(HistoryAlumni.class);
		Root<HistoryAlumni> subrootH = subqueryH.from(HistoryAlumni.class);
		subqueryH.where(
							cb.equal(root.get(HistoryAlumni_.alumni),subrootH.get(HistoryAlumni_.alumni)),
							cb.lessThan(root.get(HistoryAlumni_.send_date), subrootH.get(HistoryAlumni_.send_date))
							
					  );
		subqueryH.select(subrootH);
		
		// SUBQUERY
		Subquery<CourseRealized> subquery = cq.subquery(CourseRealized.class);
		Root<CourseRealized> subroot = subquery.from(CourseRealized.class);
		subquery.where(
							cb.equal(root.get(HistoryAlumni_.alumni),subroot.get(CourseRealized_.alumni)),
							cb.equal(subroot.get(CourseRealized_.course), course)
					  );
		subquery.select(subroot);
		cq.where(
					cb.exists(  subquery  ),
					cb.not(cb.exists(subqueryH))
				);
		
		
		cq.select(root);	
		return  em.createQuery(cq).getResultList();
		
		
	}

	
	
	@Override
	public List<HistoryAlumni> retrieveAllMine(Academic academic) {
		// Using the entity manager, create a criteria query to retrieve all objects of the domain class.
				EntityManager em = getEntityManager();
				CriteriaBuilder cb = em.getCriteriaBuilder();
				CriteriaQuery<HistoryAlumni> cq = cb.createQuery(getDomainClass());
				Root<HistoryAlumni> root = cq.from(getDomainClass());
				
				cq.where(  cb.equal(root.get(HistoryAlumni_.alumni), academic));
				
				cq.select(root);

				// Applies ordering.
				applyOrdering(cb, root, cq);

				// Return the list of objects.
				List<HistoryAlumni> result = em.createQuery(cq).getResultList();
				return result;
				//return super.retrieveAll();
	}
	
	
}
