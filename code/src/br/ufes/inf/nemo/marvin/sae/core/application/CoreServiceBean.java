package br.ufes.inf.nemo.marvin.sae.core.application;

import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.inject.Named;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;

import br.ufes.inf.nemo.marvin.core.application.CoreInformation;
import br.ufes.inf.nemo.marvin.core.domain.MarvinConfiguration;
import br.ufes.inf.nemo.marvin.sae.core.domain.SaeConfiguracao;

/**
 * Singleton bean that stores in memory information that is useful for the entire application, i.e., read-only
 * information shared by all users. This bean stores information for the core package.
 * 
 * @author Bruno Manzoli (manzoli2122@gmail.com)
 */
@Singleton
@Named
public class CoreServiceBean implements CoreService {
	
	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	
	/** The information singleton for the core module. */
	@EJB    	
	private CoreInformation coreInformation;
	
	/** The logger. */
	private static final Logger logger = Logger.getLogger(CoreService.class.getCanonicalName());

	
	/** The DAO for SaeConfiguracao objects. */
	//@EJB
	//private SaeConfiguracaoDAO saeConfiguracaoDAO;
	
	
	/** The system configuration objects */
	private SaeConfiguracao currentConfig;
	
	
	/** Getter for SaeConfiguracao. */
	public SaeConfiguracao getCurrentConfig() { return currentConfig; }


	@Override
	public void sendEmailUpdate() {
		MultiPartEmail email;
		
		String emailAddress = "manzoli2122@gmail.com";
		MarvinConfiguration config = coreInformation.getCurrentConfig(); 
		
		String msg = "Gostariamos que atualiza-se seus dados em nosso sistema\n";
		
		try{
			 email = new MultiPartEmail();
			 email.setHostName(config.getSmtpServerAddress());
			 email.setSmtpPort(config.getSmtpServerPort());
			 email.setAuthenticator(new DefaultAuthenticator(config.getSmtpUsername(), config.getSmtpPassword()));
			 email.setTLS(true);
			 email.setFrom(config.getSmtpUsername(), "Marvin");
			 
			 email.setSubject("Atualização de dados");
			 email.setMsg(msg);
			 email.addTo(emailAddress);
			 email.addTo(config.getSmtpUsername());
			 email.send();
			 
		
		}
		catch (EmailException e) {
			e.printStackTrace();
		}	
		
	}

	
	
	

	

}
