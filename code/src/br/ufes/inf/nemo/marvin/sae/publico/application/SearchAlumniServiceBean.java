package br.ufes.inf.nemo.marvin.sae.publico.application;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.marvin.sae.core.domain.CourseRealized;
import br.ufes.inf.nemo.marvin.sae.core.persistence.CourseRealizedDAO;

@Stateless
public class SearchAlumniServiceBean implements SearchAlumniService{
	
	
	/** The DAO for Academic objects. */
	@EJB    	
	private CourseRealizedDAO courseRealizedDAO;
	
	@Override
	public List<CourseRealized> getAlumnis(Course course){
		
		if(course==null){
			return  courseRealizedDAO.retrieveAll();
		}
		return  courseRealizedDAO.retrieveAllCourse(course);
		
		
	}
	

}
