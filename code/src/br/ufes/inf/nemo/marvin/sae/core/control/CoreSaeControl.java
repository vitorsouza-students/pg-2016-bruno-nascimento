package br.ufes.inf.nemo.marvin.sae.core.control;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.convert.Converter;
import javax.inject.Named;

import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.marvin.core.persistence.AcademicDAO;
import br.ufes.inf.nemo.marvin.core.persistence.CourseDAO;
import br.ufes.inf.nemo.marvin.people.domain.Gender;
import br.ufes.inf.nemo.marvin.sae.core.domain.TopicOfInterest;
import br.ufes.inf.nemo.marvin.sae.core.domain.CourseRealized;
import br.ufes.inf.nemo.marvin.sae.core.persistence.TopicOfInterestDAO;
import br.ufes.inf.nemo.marvin.sae.core.persistence.CourseRealizedDAO;
import br.ufes.inf.nemo.jbutler.ejb.controller.PersistentObjectConverterFromId;


/**
 * Application-scoped bean that centralizes controller information for the core package. This bean differs from the
 * singleton EJB CoreService by containing data relative to the presentation layer (controller and view, i.e., the
 * web).
 * 
 * @author Bruno Manzoli (manzoli2122@gmail.com)
 */
@Named
@ApplicationScoped
public class CoreSaeControl  implements Serializable {

	
	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	
	/** The logger. */
	private static final Logger logger = Logger.getLogger(CoreSaeControl.class.getCanonicalName());
	
	
	
	/** The DAO for Administrador objects. */
	@EJB    	
	private AcademicDAO academicDAO;
	
	
	/** The DAO for Assunto_Interesse objects. */
	@EJB
	private TopicOfInterestDAO topicOfInterestDAO;
	
	
	/** The DAO for Course objects. */
	@EJB
	private CourseDAO courseDAO;

	
	/** The DAO for Egresso objects. */
	@EJB
	private CourseRealizedDAO courseRealizedDAO;
	
	
		
	
	
	
	
	/** JSF Converter for Assunto_Interesse objects. */
	private PersistentObjectConverterFromId<TopicOfInterest> topicOfInterestConverter;
	
	
	/** JSF Converter for Course objects. */
	private PersistentObjectConverterFromId<Course> courseConverter;
	
	
	/** JSF Converter for Course objects. */
	private PersistentObjectConverterFromId<CourseRealized> courseRealizedConverter;


	
	
	
	public Gender[] getGender() {
		return Gender.values();
	}
	
	
	
	
	
	/** Getter for Assunto_InteresseConverter */
	public Converter getTopicOfInterestConverter() {
		if (topicOfInterestConverter == null) {
			logger.log(Level.FINEST, "Creating a assunto_Interesse converter ....... ");
			topicOfInterestConverter = new PersistentObjectConverterFromId<TopicOfInterest>(topicOfInterestDAO);
		}
		return topicOfInterestConverter;
	}
	
	
	
	/** Getter for CursoConverter */
	public Converter getCourseConverter() {
		if (courseConverter == null) {
			logger.log(Level.FINEST, "Creating a course converter ....... ");
			courseConverter = new PersistentObjectConverterFromId<Course>(courseDAO);
		}	
		return courseConverter;
	}

	
	
	/** Getter for CursoConverter */
	public Converter getCourseRealizedConverter() {
		if (courseRealizedConverter == null) {
			logger.log(Level.FINEST, "Creating a cursoRealizado converter ....... ");
			courseRealizedConverter = new PersistentObjectConverterFromId<CourseRealized>(courseRealizedDAO);
		}	
		return courseRealizedConverter;
	}
	
	
}
