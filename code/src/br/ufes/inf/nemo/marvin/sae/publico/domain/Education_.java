package br.ufes.inf.nemo.marvin.sae.publico.domain;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport_;
import br.ufes.inf.nemo.marvin.core.domain.Academic;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-08T11:58:51.178-0300")
@StaticMetamodel(Education.class)
public class Education_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<Education, Academic> academic;
	public static volatile SingularAttribute<Education, Degree_Education> degree;
	public static volatile SingularAttribute<Education, String> state;
	public static volatile SingularAttribute<Education, String> country;
	public static volatile SingularAttribute<Education, String> institution;
	public static volatile SingularAttribute<Education, Integer> year;
}
