package br.ufes.inf.nemo.marvin.sae.core.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport;

@Entity
public class CourseRealized extends PersistentObjectSupport implements Comparable<CourseRealized> {

	
	private static final long serialVersionUID = 1L;

	
	/** MATRICULA*/
	@NotNull 
	@Size(max = 10)
	private String registration;
	
	
	@NotNull 
	private int startYear;
	
	@NotNull 
	private int endYear ;
	
	@NotNull
	@ManyToOne
	private Course course;
	
	@NotNull
	@ManyToOne
	private Academic alumni;
	 
	
	
	
	@Override
	public String toString() { return "Course : " + course + ", Alumni : " + alumni ; }

	
	@Override
	 public int compareTo(CourseRealized o) {
		if (registration == null)	return 1;
		if (o.registration == null) return -1;
		int cmp = registration.compareTo(o.registration);
		if (cmp != 0 ) return cmp;
		return super.compareTo(o);	
	 }


	

	public String getRegistration() {
		return registration;
	}


	public void setRegistration(String registration) {
		this.registration = registration;
	}


	public int getStartYear() {
		return startYear;
	}


	public void setStartYear(int startYear) {
		this.startYear = startYear;
	}


	public int getEndYear() {
		return endYear;
	}


	public void setEndYear(int endYear) {
		this.endYear = endYear;
	}


	public Course getCourse() {
		return course;
	}


	public void setCourse(Course course) {
		this.course = course;
	}


	public Academic getAlumni() {
		return alumni;
	}


	public void setAlumni(Academic alumni) {
		this.alumni = alumni;
	}

	
	
 }




