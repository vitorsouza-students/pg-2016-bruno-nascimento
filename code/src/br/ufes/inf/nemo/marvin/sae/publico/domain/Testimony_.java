package br.ufes.inf.nemo.marvin.sae.publico.domain;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport_;
import br.ufes.inf.nemo.marvin.sae.core.domain.CourseRealized;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-08T11:59:33.539-0300")
@StaticMetamodel(Testimony.class)
public class Testimony_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<Testimony, Date> send_date;
	public static volatile SingularAttribute<Testimony, String> content;
	public static volatile SingularAttribute<Testimony, Boolean> anonymous;
	public static volatile SingularAttribute<Testimony, CourseRealized> courseRealized;
	public static volatile SingularAttribute<Testimony, StatusTestimony> status;
}
