package br.ufes.inf.nemo.marvin.sae.publico.domain;

public enum Acting_Area {
	EMPREENDEDOR("Empreendedor"),
	FUNCIONARIO_PUBLICO("Funcionário Público"),
	FUNCIONARIO_PRIVADO("Funcionário Privado"),
	PROFESSOR("Professor"),
	PESQUISADOR("Pesquisador");
	
	
	private final String label;

	private Acting_Area(String label) {
		this.label = label;
	}

	public String getLabel() {
		return this.label;
    }

}
