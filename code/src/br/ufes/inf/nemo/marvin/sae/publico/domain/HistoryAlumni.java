package br.ufes.inf.nemo.marvin.sae.publico.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport;




/**
 * CLASSE DE DOMMINIO QUE REPRESENTA O HISTORICO DO EGRESSO.
 * 
 * IMPLEMENTADA COM BASE NO Documento de Análise de Requisitos VERSÃO 1.4
 * 
 * <i>ESTA CLASSE FAZ PARTE DO SISTEMA SAE.</i>
 * 
 * @author BRUNO MANZOLI (manzoli2122@gmail.com)
 */

@Entity
public class HistoryAlumni  extends PersistentObjectSupport implements Comparable<HistoryAlumni> {

	
	private static final long serialVersionUID = 1L;
	
	
	/** EGRESSO DO HISTORICO */
	@NotNull
	@ManyToOne
	private Academic alumni;
		
	
	/** DATA DE ENVIO DO HSTORICO */
	@NotNull
	@Temporal(TemporalType.DATE)
	private Date send_date;
	
	
	/** FAIXA SALARIAL DO EGREESO */
	@NotNull
	private Salary_Level salary_Level;
	
	
	@NotNull
	private Acting_Area  acting_Area ;
	
	
	/** SE O EGREESO RESIDE NO ES */
	@NotNull
	private Boolean lives_in_ES;
	
	
	@NotNull
	private Degree_Education degree_Education;
	
	
	
	
	/** SE O EGREESO ATUA NA AREA DE INFORMATICA */
	@NotNull
	private Formation_Area operates_in_area_formation;
	
	
	
	
	
	
	@Override
	public int compareTo(HistoryAlumni  o) { 
		if (alumni == null)	return 1;
		if (o.alumni == null) return -1;
		int cmp = alumni.compareTo(o.alumni);
		if (cmp != 0 ) return cmp;
		
		if (send_date == null)	return 1;
		if (o.send_date == null) return -1;
		int cmpcpf = send_date.compareTo(o.send_date);
		if (cmpcpf != 0) return cmpcpf;
		
		return super.compareTo(o);
		
	}
	
	@Override
	public String toString() { return alumni.toString(); }

	
	
	
	/**  GETS AND SETS  */
	public Academic getAlumni() {return alumni;	}
	public void setAlumni(Academic alumni) {this.alumni = alumni;}

	public Formation_Area getOperates_in_area_formation() {
		return operates_in_area_formation;
	}
	public void setOperates_in_area_formation(Formation_Area operates_in_area_formation) {
		this.operates_in_area_formation = operates_in_area_formation;
	}

	public Date getSend_date() {return send_date;}
	public void setSend_date(Date send_date) {	this.send_date = send_date;	}

	public Salary_Level getSalary_Level() {	return salary_Level;}
	public void setSalary_Level(Salary_Level salary_Level) {this.salary_Level = salary_Level;}

	public Acting_Area getActing_Area() {return acting_Area;}
	public void setActing_Area(Acting_Area acting_Area) {this.acting_Area = acting_Area;}

	public Boolean getLives_in_ES() { return lives_in_ES;}
	public void setLives_in_ES(Boolean lives_in_ES) {this.lives_in_ES = lives_in_ES;}

	public Degree_Education getDegree_Education() { return degree_Education; }
	public void setDegree_Education(Degree_Education degree_Education) { this.degree_Education = degree_Education;}

}
