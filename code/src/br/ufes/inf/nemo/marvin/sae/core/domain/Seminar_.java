package br.ufes.inf.nemo.marvin.sae.core.domain;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport_;
import br.ufes.inf.nemo.marvin.core.domain.Academic;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-06-22T13:49:42.765-0300")
@StaticMetamodel(Seminar.class)
public class Seminar_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<Seminar, String> panelist;
	public static volatile SingularAttribute<Seminar, Date> date_time;
	public static volatile SingularAttribute<Seminar, String> place;
	public static volatile SingularAttribute<Seminar, String> title;
	public static volatile SingularAttribute<Seminar, TopicOfInterest> topicOfInterest;
	public static volatile SingularAttribute<Seminar, Academic> alumni_panelist;
	public static volatile SingularAttribute<Seminar, Boolean> confirmada;
}
