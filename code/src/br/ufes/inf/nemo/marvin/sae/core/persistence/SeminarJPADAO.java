package br.ufes.inf.nemo.marvin.sae.core.persistence;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.ufes.inf.nemo.marvin.sae.core.domain.Seminar;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseJPADAO;

@Stateless
public class SeminarJPADAO extends BaseJPADAO<Seminar> implements SeminarDAO{

	private static final long serialVersionUID = 1L;
	
	
	@PersistenceContext(unitName="Marvin")
	private EntityManager entityManager;
	
	
	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}

}
