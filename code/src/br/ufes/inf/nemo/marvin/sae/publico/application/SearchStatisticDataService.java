package br.ufes.inf.nemo.marvin.sae.publico.application;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.marvin.sae.publico.domain.HistoryAlumni;



@Local
public interface SearchStatisticDataService {

	
	List<HistoryAlumni> searchHistories(Course course);

}
