package br.ufes.inf.nemo.marvin.sae.publico.control;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;

import br.ufes.inf.nemo.marvin.sae.publico.application.ManageSuggestionsService;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Suggestion;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.controller.CrudController;


/**
 * Controller class responsible for mediating the communication between user interface and application service for the
 * use case "Manage Sugestao".
 * 
 * This use case is a CRUD and, thus, the controller also uses the mini CRUD framework for EJB3.
 * 
 * @author Bruno Manzoli (manzoli2122@gmail.com)
 */
@Named
@SessionScoped
public class ManageSuggestionsControl extends CrudController<Suggestion> {

	
	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	
	/** The logger. */
	private static final Logger logger = Logger.getLogger(ManageSuggestionsControl.class.getCanonicalName());
	
	
	/** The "Manage Sugestao" service. */
	@EJB
	private ManageSuggestionsService  manageSuggestionService ;
	
	
	
	
	
	
	
	
	
	
	/**   CONSTRUTOR DA CLASSE */
	public ManageSuggestionsControl(){
		 viewPath = "/sae/public/alumni/manageSuggestions/";
	     bundleName = "msgsSAE";
	}
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#getCrudService() */
	@Override
	protected CrudService<Suggestion> getCrudService() {
		manageSuggestionService.authorize();
		return manageSuggestionService;
	}

	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#createNewEntity() */
	@Override
	protected Suggestion createNewEntity() {
		logger.log(Level.FINER, "INITIALIZING AN EMPTY Sugestao  ......");
		Suggestion sugestao = new Suggestion();
		sugestao.setSend_date(new Date());
		return sugestao;
	}

	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#initFilters() */
	@Override
	protected void initFilters() {	}

	
	
	
	@Override
	protected void retrieveEntities() {		
		if (lastEntityIndex > entityCount) lastEntityIndex = (int) entityCount;
		entities = manageSuggestionService.retrieveAllMine();
		lastEntityIndex = firstEntityIndex + entities.size();		
	}
	
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#delete() */
	@Override
	public String delete() {
		try{
			return super.delete();
		}
		catch(Exception e){
			addGlobalI18nMessage(getBundleName(), FacesMessage.SEVERITY_ERROR, getBundlePrefix() + ".error.delete", summarizeSelectedEntity());
			cancelDeletion();
			return null;
		}
	}
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#save() */
	@Override
	public String save() {
		try{
			return super.save();
		}
		catch(Exception e){
			addGlobalI18nMessage(getBundleName(), FacesMessage.SEVERITY_ERROR, getBundlePrefix() + ".error.save", summarizeSelectedEntity());
			return null;
		}
	}

}
