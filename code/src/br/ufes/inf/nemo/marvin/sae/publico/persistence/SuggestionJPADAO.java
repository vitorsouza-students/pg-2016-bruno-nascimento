package br.ufes.inf.nemo.marvin.sae.publico.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.marvin.sae.core.domain.CourseRealized;
import br.ufes.inf.nemo.marvin.sae.core.domain.CourseRealized_;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Suggestion;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Suggestion_;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseJPADAO;

@Stateless
public class SuggestionJPADAO extends BaseJPADAO<Suggestion> implements SuggestionDAO{

	private static final long serialVersionUID = 1L;

	@PersistenceContext(unitName="Marvin")
	private EntityManager entityManager;

	
	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	
	@Override
	protected List<Order> getOrderList(CriteriaBuilder cb, Root<Suggestion> root) {
		List<Order> orderList = new ArrayList<Order>();
		orderList.add(cb.asc(root.get(Suggestion_.send_date)));
		return orderList;
	}
	
	
	

	@Override
	public List<Suggestion> retrieveAllMine(Academic autor) {
		EntityManager em = getEntityManager();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Suggestion> cq = cb.createQuery(getDomainClass());
		Root<Suggestion> root = cq.from(getDomainClass());
		
		
		
		Subquery<CourseRealized> subqueryH = cq.subquery(CourseRealized.class);
		Root<CourseRealized> subrootH = subqueryH.from(CourseRealized.class);
		subqueryH.where(
							cb.equal(subrootH.get(CourseRealized_.alumni),autor)							
					  );
		
		subqueryH.select(subrootH);
		
		
		cq.where(
					root.get(Suggestion_.courseRealized).in(subqueryH)										
				);

		cq.select(root);

		// Applies ordering.
		applyOrdering(cb, root, cq);

		// Return the list of objects.
		List<Suggestion> result = em.createQuery(cq).getResultList();
		return result;
		
	}
	

	
}
