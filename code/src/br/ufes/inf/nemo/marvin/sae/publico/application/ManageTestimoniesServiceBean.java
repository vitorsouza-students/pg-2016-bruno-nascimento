package br.ufes.inf.nemo.marvin.sae.publico.application;

import java.util.Date;
import java.util.List;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;

import br.ufes.inf.nemo.marvin.core.application.CoreInformation;
import br.ufes.inf.nemo.marvin.core.application.SessionInformation;
import br.ufes.inf.nemo.marvin.core.domain.MarvinConfiguration;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Testimony;
import br.ufes.inf.nemo.marvin.sae.publico.domain.StatusTestimony;
import br.ufes.inf.nemo.marvin.sae.publico.persistence.TestimonyDAO;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudException;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudServiceBean;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;



/**
 * Stateless session bean implementing the "Manage Depoimento" use case component. See the implemented interface
 * documentation for details.
 * 
 * @author Bruno Manzoli (manzoli2122@gmail.com)
 * @see ManageTestimoniesService.publico.application.ManageDepoimentoService
 */
@Stateless
@DeclareRoles({ "Admin" , "Alumni" , "Researcher" , "Student" , "Teacher" })
@RolesAllowed({ "Admin" , "Alumni" })
public class ManageTestimoniesServiceBean extends CrudServiceBean<Testimony> implements ManageTestimoniesService{

	
	/** Serialization id. */
	private static final long serialVersionUID = 1L;

	/** The information singleton for the core module. */
	@EJB    	
	private CoreInformation coreInformation;
	
	/** The DAO for Depoimento objects. */
	@EJB
	private TestimonyDAO testimonyDAO;
	
	

	@EJB
	private SessionInformation sessionInformation;
	
	
	
	
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.application.CrudService#getDAO() */
	@Override
	public BaseDAO<Testimony> getDAO() {
		return testimonyDAO;
	}

	
	
	
	
	/** @see sae.core.application.CrudServiceBean#validateCreate(br.ufes.inf.nemo.util.ejb3.persistence.PersistentObject) */
	@Override
	public void validateCreate(Testimony entity) throws CrudException {
		entity.setStatus(StatusTestimony.PENDENTE);
		entity.setSend_date(new Date());
		
		MultiPartEmail email;
		
		String emailAddress = "manzoli2122@gmail.com";
		MarvinConfiguration config = coreInformation.getCurrentConfig(); 
		
		String msg = "Foi cadastro um novo depoimento para o curso "
					+  entity.getCourseRealized().getCourse().getName()
					+ ", com o seguinte conteúdo: \n '' " 
					+ entity.getContent() +" ''. \nPara poder avaliar o depoimento entre no sistema.";
		
		try{
			 email = new MultiPartEmail();
			 email.setHostName(config.getSmtpServerAddress());
			 email.setSmtpPort(config.getSmtpServerPort());
			 email.setAuthenticator(new DefaultAuthenticator(config.getSmtpUsername(), config.getSmtpPassword()));
			 email.setTLS(true);
			 email.setFrom(config.getSmtpUsername(), "Marvin");
			 
			 email.setSubject("Cadastro de Depoimento");
			 email.setMsg(msg);
			 email.addTo(emailAddress);
			 email.addTo(config.getSmtpUsername());
			 email.send();
			 
		
		}
		catch (EmailException e) {
			e.printStackTrace();
		}	
		
		
	}
	
	
	
	@Override
	public void validateUpdate(Testimony entity) throws CrudException {
		entity.setStatus(StatusTestimony.PENDENTE);
		
		MultiPartEmail email;
		
		String emailAddress = "manzoli2122@gmail.com";
		MarvinConfiguration config = coreInformation.getCurrentConfig(); 
		
		String msg = "Foi alterado um depoimento para o curso "
					+  entity.getCourseRealized().getCourse().getName()
					+ ", para o seguinte conteúdo: \n '' " 
					+ entity.getContent() +" ''. \nPara poder avaliar este depoimento entre no sistema.";
		
		try{
			 email = new MultiPartEmail();
			 email.setHostName(config.getSmtpServerAddress());
			 email.setSmtpPort(config.getSmtpServerPort());
			 email.setAuthenticator(new DefaultAuthenticator(config.getSmtpUsername(), config.getSmtpPassword()));
			 email.setTLS(true);
			 email.setFrom(config.getSmtpUsername(), "Marvin");
			 
			 email.setSubject("Alteração de Depoimento");
			 email.setMsg(msg);
			 email.addTo(emailAddress);
			 email.addTo(config.getSmtpUsername());
			 email.send();
			 
		
		}
		catch (EmailException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	@Override
	public void approve(Testimony entity){
		entity.setStatus(StatusTestimony.APROVADO);
		update(entity);
	}

	@Override
	public void disapprove(Testimony entity){
		entity.setStatus(StatusTestimony.DESAPROVADO);
		update(entity);
		MultiPartEmail email;
		
		String emailAddress = "manzoli2122@gmail.com";
		MarvinConfiguration config = coreInformation.getCurrentConfig(); 
		
		String msg = "Seu depoimento para o curso "
					+  entity.getCourseRealized().getCourse().getName()
					+ ", com o seguinte conteúdo: \n '' " 
					+ entity.getContent() +" ''. \nNão foi aprovado, você pode altera-lo pelo sistema para ser novamente avaliado.";
		
		try{
			 email = new MultiPartEmail();
			 email.setHostName(config.getSmtpServerAddress());
			 email.setSmtpPort(config.getSmtpServerPort());
			 email.setAuthenticator(new DefaultAuthenticator(config.getSmtpUsername(), config.getSmtpPassword()));
			 email.setTLS(true);
			 email.setFrom(config.getSmtpUsername(), "Marvin");
			 
			 email.setSubject("Depoimento no Marvin não aprovado");
			 email.setMsg(msg);
			 email.addTo(emailAddress);
			 email.addTo(config.getSmtpUsername());
			 email.send();
			 
		
		}
		catch (EmailException e) {
			e.printStackTrace();
		}
		
	}
	
	
	@Override
	public List<Testimony>	retrieveAllMine() {
		return testimonyDAO.retrieveAllMine(sessionInformation.getCurrentUser());
	}
	
	
	
	@Override
	public List<Testimony>	retrieveAllAnalyze() {
		return testimonyDAO.retrieveAllAnalyze(sessionInformation.getCurrentUser());
		
	}
	
	

}
