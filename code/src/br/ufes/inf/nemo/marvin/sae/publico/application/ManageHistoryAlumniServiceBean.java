package br.ufes.inf.nemo.marvin.sae.publico.application;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.marvin.core.application.SessionInformation;
import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.marvin.sae.publico.domain.HistoryAlumni;
import br.ufes.inf.nemo.marvin.sae.publico.persistence.HistoryAlumniDAO;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudException;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudServiceBean;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;




/**
 * Stateless session bean implementing the "Manage Historico" use case component. See the implemented interface
 * documentation for details.
 * 
 * @author Bruno Manzoli (manzoli2122@gmail.com)
 * @see ManageHistoryAlumniService.publico.application.ManageHistoricoService
 */
@Stateless
@DeclareRoles({ "Admin" , "Alumni" , "Researcher" , "Student" , "Teacher" })
@RolesAllowed({ "Admin" , "Alumni" })
public class ManageHistoryAlumniServiceBean extends CrudServiceBean<HistoryAlumni> implements ManageHistoryAlumniService{

	
	/** Serialization id. */
	private static final long serialVersionUID = 1L;

	
	/** The DAO for Historico_Egresso objects. */
	@EJB
	private HistoryAlumniDAO historyAlumniDAO;
	
	
	@EJB
	private SessionInformation sessionInformation;
	
	
	
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.application.CrudService#getDAO() */
	@Override
	public BaseDAO<HistoryAlumni> getDAO() {
		return historyAlumniDAO;
	}

	
	

	
	
	/** @see sae.core.application.CrudServiceBean#validateCreate(br.ufes.inf.nemo.util.ejb3.persistence.PersistentObject) */
	@Override
	public void validateCreate(HistoryAlumni entity) throws CrudException {
		Academic academic = sessionInformation.getCurrentUser();
		if(academic != null)
			entity.setAlumni(academic);
		
		entity.setSend_date(new Date());
		
	}
	
	
	
	@Override
	public void authorize() {
		super.authorize();
	}
	
	
	
	@Override
	public void validateDelete(HistoryAlumni entity) throws CrudException {
		
		if(!isAlteravel(entity)){
			int x=3/0;
		}
		
		
	}
	
	
	
	@Override
	public void validateUpdate(HistoryAlumni entity) throws CrudException {
		
		CrudException crudException = null;
		String crudExceptionMessage =  "this object cannot be updated due to validation errors.";

		if(!isAlteravel(entity)){
			crudException = addValidationError(crudException, crudExceptionMessage, null , "manageHistoricoControl.error.update", entity.getSend_date());
		}
		
		if (crudException != null){
			throw crudException;
		}
		
	}
	
	
	
	@Override
	public List<HistoryAlumni>	retrieveAllMine() {
		return historyAlumniDAO.retrieveAllMine(sessionInformation.getCurrentUser());
	
	}



	@Override
	public boolean isAlteravel(HistoryAlumni historico) {
		Date hoje = new Date();
		if(historico!=null){
			
			Calendar dInicial = Calendar.getInstance(); 
	        dInicial.setTime(historico.getSend_date());
	        Calendar dFinal = Calendar.getInstance();
	        dFinal.setTime( hoje);
	        
	        int MILLIS_IN_DAY = 86400000;
	        int dif =  (int) ((dFinal.getTimeInMillis() - dInicial.getTimeInMillis()) / MILLIS_IN_DAY);
	        
	       
			if(dif > 10){
				return false;
			}
		}
		return true;
	}
}
