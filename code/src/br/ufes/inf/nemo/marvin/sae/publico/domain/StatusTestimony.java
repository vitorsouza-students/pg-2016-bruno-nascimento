package br.ufes.inf.nemo.marvin.sae.publico.domain;

public enum StatusTestimony {

	
	PENDENTE("Pendente"),
	APROVADO("Aprovada"),
	DESAPROVADO("Desaprovada");
	
	
	private final String label;

	private StatusTestimony(String label) {
		this.label = label;
	}

	public String getLabel() {
		return this.label;
    }
	
	
}
