package br.ufes.inf.nemo.marvin.sae.publico.application;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.marvin.sae.core.domain.CourseRealized;

@Local
public interface SearchAlumniService {

	List<CourseRealized> getAlumnis(Course course);

}
