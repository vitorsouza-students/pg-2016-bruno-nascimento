package br.ufes.inf.nemo.marvin.sae.core.domain;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport_;
import br.ufes.inf.nemo.marvin.core.domain.Academic;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-06-29T15:50:23.342-0300")
@StaticMetamodel(TopicOfInterest.class)
public class TopicOfInterest_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<TopicOfInterest, String> name;
	public static volatile SetAttribute<TopicOfInterest, Academic> academics;
}
