package br.ufes.inf.nemo.marvin.sae.publico.application;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.marvin.core.application.SessionInformation;
import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Education;
import br.ufes.inf.nemo.marvin.sae.publico.persistence.EducationDAO;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudException;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudServiceBean;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;



/**
 * Stateless session bean implementing the "Manage Escolaridade" use case component. See the implemented interface
 * documentation for details.
 * 
 * @author Bruno Manzoli (manzoli2122@gmail.com)
 * @see sae.core.application.ManageAdministradorService
 */
@Stateless
@DeclareRoles({ "Admin" , "Alumni" , "Researcher" , "Student" , "Teacher" })
@RolesAllowed({ "Admin" , "Alumni" })
public class ManageEducationsServiceBean extends CrudServiceBean<Education> implements ManageEducationsService{

	
	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	
	/** The logger. */
	private static final Logger logger = Logger.getLogger(ManageEducationsServiceBean.class.getCanonicalName());
	

	/** The DAO for Escolaridade objects. */
	@EJB
	private EducationDAO educationDAO;
	
	
	@EJB
	private SessionInformation sessionInformation;

	
	
	
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.application.CrudService#getDAO() */
	@Override
	public BaseDAO<Education> getDAO() {
		return educationDAO;
	}

	
	
	
	
	@Override
	public void authorize() {
		super.authorize();
	}
	
	
	/** @see sae.core.application.CrudServiceBean#validateCreate(br.ufes.inf.nemo.util.ejb3.persistence.PersistentObject) */
	@Override
	public void validateCreate(Education entity) throws CrudException {
		Academic academic = sessionInformation.getCurrentUser();
		entity.setAcademic(academic);
		
	}
	
	
	
	
	@Override
	public List<Education>	retrieveAllMine() {
		return educationDAO.retrieveAllMine(sessionInformation.getCurrentUser());
	}
	
	
}
