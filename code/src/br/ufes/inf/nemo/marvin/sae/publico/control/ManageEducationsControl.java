package br.ufes.inf.nemo.marvin.sae.publico.control;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;

import br.ufes.inf.nemo.marvin.sae.publico.application.ManageEducationsService;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Education;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.controller.CrudController;



/**
 * Controller class responsible for mediating the communication between user interface and application service for the
 * use case "Manage Escolaridade".
 * 
 * This use case is a CRUD and, thus, the controller also uses the mini CRUD framework for EJB3.
 * 
 * @author Bruno Manzoli (manzoli2122@gmail.com)
 */
@Named
@SessionScoped
public class ManageEducationsControl extends CrudController<Education>{

	
	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	
	/** The logger. */
	private static final Logger logger = Logger.getLogger(ManageEducationsControl.class.getCanonicalName());
	
	
	/** The "Manage Escolaridade" service. */
	@EJB
	private ManageEducationsService  manageEducationsService ;
	
	
	
	
	
	
	/**   CONSTRUTOR DA CLASSE */
	public ManageEducationsControl(){
		 viewPath = "/sae/public/alumni/manageEducations/";
	     bundleName = "msgsSAE";
	}
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#getCrudService() */
	@Override
	protected CrudService<Education> getCrudService() {
		manageEducationsService.authorize();
		return manageEducationsService;
	}

	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#initFilters() */
	@Override
	protected void initFilters() {	
	}

	
		
	@Override
	protected void retrieveEntities() {		
		if (lastEntityIndex > entityCount) 
			lastEntityIndex = (int) entityCount;
		
		entities = manageEducationsService.retrieveAllMine();
		
		lastEntityIndex = firstEntityIndex + entities.size();
				
	}
	
	

	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#delete() */
	@Override
	public String delete() {
		try{
			return super.delete();
		}
		catch(Exception e){
			addGlobalI18nMessage(getBundleName(), FacesMessage.SEVERITY_ERROR, getBundlePrefix() + ".error.delete", summarizeSelectedEntity());
			cancelDeletion();
			return null;
		}
	}
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#save() */
	@Override
	public String save() {
		try{
			return super.save();
		}
		catch(Exception e){
			addGlobalI18nMessage(getBundleName(), FacesMessage.SEVERITY_ERROR, getBundlePrefix() + ".error.save", summarizeSelectedEntity());
			return null;
		}
	}
	
		
}
