package br.ufes.inf.nemo.marvin.sae.publico.application;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.sae.publico.domain.Testimony;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;


/**
 * Local EJB interface for the component that implements the "Manage Depoimento" use case.
 * 
 * This use case consists of a CRUD for the class Depoimento and uses the mini CRUD framework for EJB3.
 * 
 * @author Bruno Manzoli (manzoli2122@gmail.com)
 * @see br.ufes.inf.nemo.util.ejb3.application.CrudService
 */
@Local
public interface ManageTestimoniesService extends CrudService<Testimony> {

	List<Testimony> retrieveAllMine();

	List<Testimony> retrieveAllAnalyze();

	void approve(Testimony entity);

	void disapprove(Testimony entity);

}
