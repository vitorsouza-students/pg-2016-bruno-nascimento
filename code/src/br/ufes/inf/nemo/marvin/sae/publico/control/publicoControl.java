package br.ufes.inf.nemo.marvin.sae.publico.control;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import br.ufes.inf.nemo.marvin.core.application.SessionInformation;
import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.marvin.sae.core.domain.TopicOfInterest;
import br.ufes.inf.nemo.marvin.sae.core.persistence.TopicOfInterestDAO;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Acting_Area;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Formation_Area;
import br.ufes.inf.nemo.marvin.sae.publico.domain.HistoryAlumni;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Salary_Level;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Degree_Education;
import br.ufes.inf.nemo.marvin.sae.publico.persistence.TestimonyDAO;



@Named
@ApplicationScoped
public class publicoControl implements Serializable{

	
	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	
	/** The logger. */
	private static final Logger logger = Logger.getLogger(publicoControl.class.getCanonicalName());
	
	
	/** The DAO for Depoimento objects. */
	@EJB    	
	private TestimonyDAO depoimentoDAO;
	
	
	/** The DAO for Assunto_Interesse objects. */
	@EJB
	private TopicOfInterestDAO topicOfInterestDAO;
	
	/** Information on the current visitor. */
	@EJB
	private SessionInformation sessionInformation;
	
	
	
	
	
	/*
	private List<TopicOfInterest> listarAssuntos;
	
	private List<TopicOfInterest> assuntos;
	
	
	public List<TopicOfInterest> listarAssuntos(){
		//if(listarAssuntos==null)
			listarAssuntos = topicOfInterestDAO.retrieveAll();
		return listarAssuntos ;
	}
	
	public String atualizar_Dados(){
		assuntos = new ArrayList<TopicOfInterest>();
		Academic usuario = sessionInformation.getCurrentUser();
		
		Iterator<TopicOfInterest> iterator = listarAssuntos().iterator();
		
		while(iterator.hasNext()){
			TopicOfInterest top = iterator.next();
			if(top.getAcademics().contains(usuario)){
				assuntos.add(top);
			}
			
		}
		
		return "/sae/public/alumni/atualizar_Dados/form.xhtml?faces-redirect=true";
	}
	
	
	public String saveDados(){
		
		Academic usuario = sessionInformation.getCurrentUser();
		Iterator<TopicOfInterest> iterator = listarAssuntos().iterator();
		
		while(iterator.hasNext()){
			TopicOfInterest top = iterator.next();
			if(assuntos.contains(top)){
				if(!top.getAcademics().contains(usuario)){
					top.getAcademics().add(usuario);
					topicOfInterestDAO.save(top);
				}
				
			}
			else{
				if(top.getAcademics().contains(usuario)){
					top.getAcademics().remove(usuario);
					topicOfInterestDAO.save(top);
				}				
			}
			
		}
		
		return "/index.xhtml?faces-redirect=true";
	}
	
	
	*/
	
	
	
	
	
	public Salary_Level[] getSalary_Level() {
		return Salary_Level.values();
	}
	
	public Acting_Area[] getActing_Area() {
		return Acting_Area.values();
	}
	
	
	public Formation_Area[] getFormation_Area() {
		return Formation_Area.values();
	}
	
	public Degree_Education[] getDegree_Education() {
		return Degree_Education.values();
	}


	//public List<TopicOfInterest> getAssuntos() { return assuntos; 	}
	//public void setAssuntos(List<TopicOfInterest> assuntos) { this.assuntos = assuntos; }

	
}
