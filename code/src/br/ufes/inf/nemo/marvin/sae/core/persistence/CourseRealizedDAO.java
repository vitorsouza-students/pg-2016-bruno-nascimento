package br.ufes.inf.nemo.marvin.sae.core.persistence;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.marvin.sae.core.domain.CourseRealized;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;

@Local
public interface CourseRealizedDAO extends BaseDAO<CourseRealized>{

	
	List<CourseRealized> retrieveMyCourses(Academic academic);

	List<CourseRealized> retrieveAllCourse(Course course);

	
}
