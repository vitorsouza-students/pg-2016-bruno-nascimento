package br.ufes.inf.nemo.marvin.sae.publico.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.marvin.core.domain.Course_;
import br.ufes.inf.nemo.marvin.sae.core.domain.CourseRealized;
import br.ufes.inf.nemo.marvin.sae.core.domain.CourseRealized_;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Testimony;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Testimony_;
import br.ufes.inf.nemo.marvin.sae.publico.domain.StatusTestimony;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseJPADAO;





@Stateless
public class TestimonyJPADAO extends BaseJPADAO<Testimony> implements TestimonyDAO{

	private static final long serialVersionUID = 1L;

	@PersistenceContext(unitName="Marvin")
	private EntityManager entityManager;
	
	
	

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	
	@Override
	protected List<Order> getOrderList(CriteriaBuilder cb, Root<Testimony> root) {
		List<Order> orderList = new ArrayList<Order>();
		orderList.add(cb.asc(root.get(Testimony_.send_date)));
		return orderList;
	}

	/*
	@Override
	public List<Testimony> retrieveAllMine(Egresso autor) {
		if(autor==null)
			return null;
		
		EntityManager em = getEntityManager();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Testimony> cq = cb.createQuery(getDomainClass());
		Root<Testimony> root = cq.from(getDomainClass());
		
		Subquery<CourseRealized> subqueryH = cq.subquery(CourseRealized.class);
		Root<CourseRealized> subrootH = subqueryH.from(CourseRealized.class);
		subqueryH.where(
							cb.equal(subrootH.get(CourseRealized_.egresso),autor)							
					  );
		
		subqueryH.select(subrootH);
		
		cq.where(
					root.get(Testimony_.CourseRealized).in(subqueryH)										
				);

		cq.select(root);

		// Applies ordering.
		applyOrdering(cb, root, cq);

		// Return the list of objects.
		List<Testimony> result = em.createQuery(cq).getResultList();
		return result;
		//return super.retrieveAll();
	}
	
	*/
	
	@Override
	public List<Testimony> retrieveAllCourse(Course course) {
		
		if(course==null)
			return null;
		
		EntityManager em = getEntityManager();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Testimony> cq = cb.createQuery(getDomainClass());
		Root<Testimony> root = cq.from(getDomainClass());
		
		Subquery<CourseRealized> subqueryH = cq.subquery(CourseRealized.class);
		Root<CourseRealized> subrootH = subqueryH.from(CourseRealized.class);
		subqueryH.where(
							cb.equal(subrootH.get(CourseRealized_.course),course)							
					  );
		
		subqueryH.select(subrootH);
		
		cq.where(
					root.get(Testimony_.courseRealized).in(subqueryH)	,	
					cb.equal(root.get(Testimony_.status), StatusTestimony.APROVADO)
				);
		
		cq.select(root);

		// Applies ordering.
		applyOrdering(cb, root, cq);

		// Return the list of objects.
		List<Testimony> result = em.createQuery(cq).getResultList();
		return result;
		//return super.retrieveAll();
	}

	
	
	@Override
	public List<Testimony> retrieveAllApproved() {
	
		
		EntityManager em = getEntityManager();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Testimony> cq = cb.createQuery(getDomainClass());
		Root<Testimony> root = cq.from(getDomainClass());
	
		
		cq.where(	
					cb.equal(root.get(Testimony_.status), StatusTestimony.APROVADO)
				);
		
		cq.select(root);

		// Applies ordering.
		applyOrdering(cb, root, cq);

		// Return the list of objects.
		List<Testimony> result = em.createQuery(cq).getResultList();
		return result;
		
	}
	
	
	
	@Override
	public List<Testimony> retrieveAllMine(Academic academic) {
		if(academic==null)
			return null;
		
		EntityManager em = getEntityManager();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Testimony> cq = cb.createQuery(getDomainClass());
		Root<Testimony> root = cq.from(getDomainClass());
		
		Subquery<CourseRealized> subqueryH = cq.subquery(CourseRealized.class);
		Root<CourseRealized> subrootH = subqueryH.from(CourseRealized.class);
		subqueryH.where(
							cb.equal(subrootH.get(CourseRealized_.alumni),academic)							
					  );
		
		subqueryH.select(subrootH);
		
		cq.where(
					root.get(Testimony_.courseRealized).in(subqueryH)										
				);

		cq.select(root);

		// Applies ordering.
		applyOrdering(cb, root, cq);

		// Return the list of objects.
		List<Testimony> result = em.createQuery(cq).getResultList();
		return result;
	}

	@Override
	public List<Testimony> retrieveAllAnalyze(Academic academic) {
		if(academic==null)
			return null;
		
		EntityManager em = getEntityManager();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		
		//Set<Course> courses=  academic.getCursosCoordenados();
		
		CriteriaQuery<Testimony> cq = cb.createQuery(getDomainClass());
		Root<Testimony> root = cq.from(getDomainClass());
		
		
		
		// SUBQUERYH
		Subquery<Course> subqueryH = cq.subquery(Course.class);
		Root<Course> subrootH = subqueryH.from(Course.class);
		subqueryH.where(
							cb.equal(subrootH.get(Course_.coordinator),academic)
							//cb.equal(subrootH,root.get(Testimony_.courseRealized))
						);
		subqueryH.select(subrootH);
		
		
		
		
		
		// SUBQUERYCR
		Subquery<CourseRealized> subqueryCR = cq.subquery(CourseRealized.class);
		Root<CourseRealized> subrootCR = subqueryCR.from(CourseRealized.class);
		subqueryCR.where(
						subrootCR.get(CourseRealized_.course).in(subqueryH)
					  );
		subqueryCR.select(subrootCR);
		
		cq.where(
					cb.equal(root.get(Testimony_.status), StatusTestimony.PENDENTE),
					root.get(Testimony_.courseRealized).in(subqueryCR)
				);
		
		cq.select(root);

		// Applies ordering.
		applyOrdering(cb, root, cq);

		// Return the list of objects.
		List<Testimony> result = em.createQuery(cq).getResultList();
		return result;
	}

	
	
	
	
}
