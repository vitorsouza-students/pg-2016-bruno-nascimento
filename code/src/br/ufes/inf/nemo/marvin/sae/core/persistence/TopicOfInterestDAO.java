package br.ufes.inf.nemo.marvin.sae.core.persistence;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.sae.core.domain.TopicOfInterest;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;

@Local
public interface TopicOfInterestDAO extends BaseDAO<TopicOfInterest>{

}
