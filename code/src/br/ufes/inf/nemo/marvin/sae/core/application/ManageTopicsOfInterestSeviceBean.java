package br.ufes.inf.nemo.marvin.sae.core.application;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.marvin.sae.core.domain.TopicOfInterest;
import br.ufes.inf.nemo.marvin.sae.core.persistence.TopicOfInterestDAO;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudServiceBean;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;



/**
 * Stateless session bean implementing the "Manage Assunto_Interesse" use case component. See the implemented interface
 * documentation for details.
 * 
 * @author Bruno Manzoli (manzoli2122@gmail.com)
 * @see sae.core.application.ManageAdministradorService
 */
@Stateless
@DeclareRoles({ "Admin" , "Alumni" , "Researcher" , "Student" , "Teacher" })
@RolesAllowed({ "Admin" , "Alumni" })
public class ManageTopicsOfInterestSeviceBean  extends CrudServiceBean<TopicOfInterest> implements ManageTopicsOfInterestSevice{

	
	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	
	/** The DAO for Assunto_Interesse objects. */
	@EJB
	private TopicOfInterestDAO topicOfInterestDAO;
	
	
	
	
	
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.application.CrudService#getDAO() */
	@Override
	public BaseDAO<TopicOfInterest> getDAO() {
		return topicOfInterestDAO;
	}

	
	
}
