package br.ufes.inf.nemo.marvin.sae.publico.domain;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport_;
import br.ufes.inf.nemo.marvin.core.domain.Academic;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-06-29T09:54:15.565-0300")
@StaticMetamodel(HistoryAlumni.class)
public class HistoryAlumni_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<HistoryAlumni, Academic> alumni;
	public static volatile SingularAttribute<HistoryAlumni, Date> send_date;
	public static volatile SingularAttribute<HistoryAlumni, Salary_Level> salary_Level;
	public static volatile SingularAttribute<HistoryAlumni, Acting_Area> acting_Area;
	public static volatile SingularAttribute<HistoryAlumni, Boolean> lives_in_ES;
	public static volatile SingularAttribute<HistoryAlumni, Degree_Education> degree_Education;
	public static volatile SingularAttribute<HistoryAlumni, Formation_Area> operates_in_area_formation;
}
