package br.ufes.inf.nemo.marvin.sae.publico.application;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Testimony;
import br.ufes.inf.nemo.marvin.sae.publico.persistence.TestimonyDAO;



@Stateless
public class SearchTestimoniesServiceBean  implements SearchTestimoniesService {
	
	

	/** The DAO for Depoimento objects. */
	@EJB    	
	private TestimonyDAO testimonyDAO;
	
	
	@Override
	public List<Testimony> getTestimonies(Course course){
		
		if(course == null){
			return testimonyDAO.retrieveAllApproved();
		}
		
		return testimonyDAO.retrieveAllCourse(course);
	}

}
