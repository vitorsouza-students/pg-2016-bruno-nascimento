package br.ufes.inf.nemo.marvin.sae.publico.application;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Testimony;


@Local
public interface SearchTestimoniesService {

	List<Testimony> getTestimonies(Course course);

}
