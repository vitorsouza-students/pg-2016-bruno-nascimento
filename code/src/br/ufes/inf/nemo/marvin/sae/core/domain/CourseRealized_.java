package br.ufes.inf.nemo.marvin.sae.core.domain;

import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport_;
import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.marvin.core.domain.Course;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-08T11:56:36.812-0300")
@StaticMetamodel(CourseRealized.class)
public class CourseRealized_ extends PersistentObjectSupport_ {
	public static volatile SingularAttribute<CourseRealized, String> registration;
	public static volatile SingularAttribute<CourseRealized, Integer> startYear;
	public static volatile SingularAttribute<CourseRealized, Integer> endYear;
	public static volatile SingularAttribute<CourseRealized, Course> course;
	public static volatile SingularAttribute<CourseRealized, Academic> alumni;
}
