package br.ufes.inf.nemo.marvin.sae.publico.application;

import java.util.Date;
import java.util.List;
import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;

import br.ufes.inf.nemo.marvin.core.application.CoreInformation;
import br.ufes.inf.nemo.marvin.core.application.SessionInformation;
import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.marvin.core.domain.MarvinConfiguration;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Suggestion;
import br.ufes.inf.nemo.marvin.sae.publico.persistence.SuggestionDAO;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudException;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudServiceBean;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;




/**
 * Stateless session bean implementing the "Manage Sugestao" use case component. See the implemented interface
 * documentation for details.
 * 
 * @author Bruno Manzoli (manzoli2122@gmail.com)
 * @see ManageSuggestionsService.publico.application.ManageSugestaoService
 */
@Stateless
@DeclareRoles({ "Admin" , "Alumni" , "Researcher" , "Student" , "Teacher" })
@RolesAllowed({ "Admin" , "Alumni" })
public class ManageSuggestionsServiceBean extends CrudServiceBean<Suggestion> implements ManageSuggestionsService{

	
	/** Serialization id. */
	private static final long serialVersionUID = 1L;

	/** The information singleton for the core module. */
	@EJB    	
	private CoreInformation coreInformation;
	
	/** The DAO for Sugestao objects. */
	@EJB
	private SuggestionDAO suggestionDAO;
	
	
	@EJB
	private SessionInformation sessionInformation;

	
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.application.CrudService#getDAO() */
	@Override
	public BaseDAO<Suggestion> getDAO() {
		return suggestionDAO;
	}

	
	

	@Override
	public void authorize() {
		super.authorize();
	}
	
	
	@Override
	public void validateDelete(Suggestion entity) throws CrudException {
		Academic academic = sessionInformation.getCurrentUser();
		if(!academic.equals(entity.getCourseRealized().getAlumni())){
			throw new CrudException(null, null, null);
		}
	}
	
	
	@Override
	public void validateCreate(Suggestion entity) throws CrudException {
		
		entity.setSend_date(new Date());
		
		
		MultiPartEmail email;
		
		String emailAddress = "manzoli2122@gmail.com";
		MarvinConfiguration config = coreInformation.getCurrentConfig(); 
		
		String msg = "Foi cadastro uma nova sugestão para o curso "
					+  entity.getCourseRealized().getCourse().getName()
					+ ", com o seguinte conteúdo: \n '' " 
					+ entity.getContent() +" ''.";
		
		try{
			 email = new MultiPartEmail();
			 email.setHostName(config.getSmtpServerAddress());
			 email.setSmtpPort(config.getSmtpServerPort());
			 email.setAuthenticator(new DefaultAuthenticator(config.getSmtpUsername(), config.getSmtpPassword()));
			 email.setTLS(true);
			 email.setFrom(config.getSmtpUsername(), "Marvin");
			 
			 email.setSubject("Cadastro de sugestão");
			 email.setMsg(msg);
			 email.addTo(emailAddress);
			 email.addTo(config.getSmtpUsername());
			 email.send();
			 
		
		}
		catch (EmailException e) {
			e.printStackTrace();
		}	
	}
	
	
	@Override
	public List<Suggestion>	retrieveAllMine() {
		return suggestionDAO.retrieveAllMine(sessionInformation.getCurrentUser());
		
	}
	
	
	
	

}
