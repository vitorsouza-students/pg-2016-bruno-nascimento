package br.ufes.inf.nemo.marvin.sae.publico.domain;

public enum Degree_Education {

	A_SUPERIOR("Superior"),
	B_ESPECIALIZACAO("Especializacao"),
	C_MESTRADO("Mestrado"),
	D_DOUTORADO("Doutorado"),
	E_POS_DOUTORADO("Pos Doutorado");
	
	
	private final String label;

	private Degree_Education(String label) {
		this.label = label;
	}

	public String getLabel() {
		return this.label;
    }
}
