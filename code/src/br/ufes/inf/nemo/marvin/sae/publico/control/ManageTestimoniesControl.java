package br.ufes.inf.nemo.marvin.sae.publico.control;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.ufes.inf.nemo.marvin.sae.publico.application.ManageTestimoniesService;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Testimony;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.controller.CrudController;
import br.ufes.inf.nemo.jbutler.ejb.controller.PrimefacesLazyEntityDataModel;


/**
 * Controller class responsible for mediating the communication between user interface and application service for the
 * use case "Manage Depoimento".
 * 
 * This use case is a CRUD and, thus, the controller also uses the mini CRUD framework for EJB3.
 * 
 * @author Bruno Manzoli (manzoli2122@gmail.com)
 */
@Named
@SessionScoped
public class ManageTestimoniesControl extends CrudController<Testimony>{

	
	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	
	private final String VIEWPATHCORE = "/sae/core/manageTestimonies/";
	
	
	/** The logger. */
	private static final Logger logger = Logger.getLogger(ManageTestimoniesControl.class.getCanonicalName());
	
	
	/** The "Manage Depoimento" service. */
	@EJB
	private ManageTestimoniesService  manageTestimonyService ;
	
	
	
	/**   CONSTRUTOR DA CLASSE */
	public ManageTestimoniesControl(){
		 viewPath = "/sae/public/alumni/manageTestimonies/";
	     bundleName = "msgsSAE";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/** Primefaces lazy data model for use with a lazy p:dataTable component. */
	private LazyDataModel<Testimony> pendingEntities;
	
	/**
	 * Getter 
	 * 
	 * @return Primefaces lazy data model for use with a lazy p:dataTable component.
	 */
	public LazyDataModel<Testimony> getPendingEntities() {
		if (pendingEntities == null) {
			count();
			pendingEntities = new PrimefacesLazyEntityDataModel<Testimony>(getListingService().getDAO()) {
				/** Serialization id. */
				private static final long serialVersionUID = 1117380513193004406L;

				@Override
				public List<Testimony> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
					firstEntityIndex = first;
					lastEntityIndex = first + pageSize;
					retrievePendingEntities() ;
					return entities;
				}
			};
			pendingEntities.setRowCount((int) entityCount);
		}
		return pendingEntities;
	}
	
	
	
	protected void retrievePendingEntities() {		
		if (lastEntityIndex > entityCount) lastEntityIndex = (int) entityCount;
		entities = manageTestimonyService.retrieveAllAnalyze();
		lastEntityIndex = firstEntityIndex + entities.size();		
	}
	
	
	public String analyzeTestimonies(){	
		selectedEntity = null;
		count();
		retrievePendingEntities();
		return VIEWPATHCORE + "list.xhtml?faces-redirect=" + getFacesRedirect();
	}
	
	
	
	public String retrieveAnalyze() {
		logger.log(Level.INFO, "Displaying form for entity retrieval");
		readOnly = true;
		if (selectedEntity == null) return null;
		else {
			// Asks the CRUD service to fetch any lazy collection that possibly exists.
			selectedEntity = getCrudService().fetchLazy(selectedEntity);
			checkSelectedEntity();
		}
		// Goes to the form.
		return VIEWPATHCORE  + "form.xhtml?faces-redirect=" + getFacesRedirect();
	}
	
	
	
	public String approve(){
		manageTestimonyService.approve(selectedEntity);;
		return analyzeTestimonies();
	}

	
	
	public String disapprove(){
		manageTestimonyService.disapprove(selectedEntity);
		return analyzeTestimonies();
	}
	
	
	
	
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#getCrudService() */
	@Override
	protected CrudService<Testimony> getCrudService() {
		manageTestimonyService.authorize();
		return manageTestimonyService;
	}



	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#initFilters() */
	@Override
	protected void initFilters() {
	}

	
	
	@Override
	protected void retrieveEntities() {		
		if (lastEntityIndex > entityCount) lastEntityIndex = (int) entityCount;
		entities = manageTestimonyService.retrieveAllMine();
		lastEntityIndex = firstEntityIndex + entities.size();		
	}
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#delete() */
	@Override
	public String delete() {
		try{
			return super.delete();
		}
		catch(Exception e){
			addGlobalI18nMessage(getBundleName(), FacesMessage.SEVERITY_ERROR, getBundlePrefix() + ".error.delete", summarizeSelectedEntity());
			cancelDeletion();
			return null;

		}
	}
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#save() */
	@Override
	public String save() {
		try{
			return super.save();
		}
		catch(Exception e){
			addGlobalI18nMessage(getBundleName(), FacesMessage.SEVERITY_ERROR, getBundlePrefix() + ".error.save" , summarizeSelectedEntity()  );
			return null;
		}
	}
	
	
}
