package br.ufes.inf.nemo.marvin.sae.publico.persistence;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Salary_Level;
import br.ufes.inf.nemo.marvin.sae.publico.domain.HistoryAlumni;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;

@Local
public interface HistoryAlumniDAO  extends BaseDAO<HistoryAlumni>{

	long contFaixaSalarial(Salary_Level faixa, Course course);

	long contReside(Boolean mora, Course course);

	
	List<HistoryAlumni> retrieveAllMine(Academic academic);

	List<HistoryAlumni> searchHistories(Course course);

	
}
