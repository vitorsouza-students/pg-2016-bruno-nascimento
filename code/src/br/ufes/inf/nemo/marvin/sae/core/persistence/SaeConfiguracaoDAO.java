package br.ufes.inf.nemo.marvin.sae.core.persistence;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.sae.core.domain.SaeConfiguracao;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;
import br.ufes.inf.nemo.jbutler.ejb.persistence.exceptions.PersistentObjectNotFoundException;

@Local
public interface SaeConfiguracaoDAO extends BaseDAO<SaeConfiguracao> {

	SaeConfiguracao retrieveCurrentConfiguration() throws PersistentObjectNotFoundException;

}
