package br.ufes.inf.nemo.marvin.sae.core.application;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.sae.core.domain.Seminar;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;

@Local
public interface ManageSeminarsService  extends CrudService<Seminar> {

	

	void confirmSeminar(Seminar seminar);

	void invitePanelist(Seminar seminar);

	void volunteerPanelist(Seminar seminar);

}
