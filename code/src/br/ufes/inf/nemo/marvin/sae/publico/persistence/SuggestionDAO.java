package br.ufes.inf.nemo.marvin.sae.publico.persistence;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Suggestion;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;

@Local
public interface SuggestionDAO extends BaseDAO<Suggestion> {

	List<Suggestion> retrieveAllMine(Academic autor);

	
	
}
