package br.ufes.inf.nemo.marvin.sae.publico.persistence;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Testimony;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;

@Local
public interface TestimonyDAO extends BaseDAO<Testimony>{

	List<Testimony> retrieveAllMine(Academic academic);

	List<Testimony> retrieveAllAnalyze(Academic academic);

	List<Testimony> retrieveAllApproved();

	List<Testimony> retrieveAllCourse(Course course);

}
