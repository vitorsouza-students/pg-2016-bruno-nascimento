package br.ufes.inf.nemo.marvin.sae.publico.control;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.marvin.sae.publico.application.SearchTestimoniesService;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Testimony;

@Named
@SessionScoped
public class SearchTestimoniesControl implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	private Course course;
	
	private List<Testimony> testimonies;
	
	protected String viewPath;
	
	protected String bundleName;
	
	@EJB 
	private SearchTestimoniesService searchTestimoniesService;
	
	
	
	/*   CONSTRUTOR DA CLASSE */
	public SearchTestimoniesControl(){
		 viewPath = "/sae/public/search/testimony/";
	     bundleName = "msgsSAE";
	}
	
	
	public boolean getFacesRedirect() { return true; }
	public String getViewPath() {return viewPath;	}
	
	
	
	public String testimony(){
		course = null;
		return getViewPath()  + "index.xhtml?faces-redirect=" + getFacesRedirect();
	}
	
	public String showTestimonies(){
		return getViewPath()  + "testimonies.xhtml?faces-redirect=" + getFacesRedirect();
	}
	
	
	
	public Course getCourse() { return course; }
	public void setCourse(Course course) { this.course = course; }
	
	
	
	public List<Testimony> getTestimonies(){
		testimonies = searchTestimoniesService.getTestimonies(course);
		return testimonies ;
	}
	
}
