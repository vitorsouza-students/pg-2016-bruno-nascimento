package br.ufes.inf.nemo.marvin.sae.publico.application;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Salary_Level;
import br.ufes.inf.nemo.marvin.sae.publico.domain.HistoryAlumni;
import br.ufes.inf.nemo.marvin.sae.publico.persistence.HistoryAlumniDAO;




@Stateless
public class SearchStatisticDataServiceBean implements SearchStatisticDataService{

	
	@EJB
	private HistoryAlumniDAO historico_EgressoDAO;
	
	
	
	public long countFaixaSalarial(Salary_Level faixa, Course course){
		return historico_EgressoDAO.contFaixaSalarial(faixa, course);
	}
	
	
	
	public long countReside(Boolean mora, Course course){
		return historico_EgressoDAO.contReside(mora, course);
	}
	
	
	@Override
	public List<HistoryAlumni> searchHistories(Course course){
		return historico_EgressoDAO.searchHistories(course);
	}
	
	
}
