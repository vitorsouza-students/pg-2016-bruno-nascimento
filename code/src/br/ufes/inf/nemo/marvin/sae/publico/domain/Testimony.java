package br.ufes.inf.nemo.marvin.sae.publico.domain;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import br.ufes.inf.nemo.marvin.sae.core.domain.CourseRealized;
import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport;


@Entity
public class Testimony extends PersistentObjectSupport implements Comparable<Testimony> {

	private static final long serialVersionUID = 1L;
	
	
	/* NOME DO USUARIO */
	@NotNull
	private Date send_date;
	
	
	/* NOME DO USUARIO */
	// @Column(length=1000)
	@NotNull
	@Lob
	private String content;
	
	
	/* NOME DO USUARIO */
	@NotNull
	private Boolean anonymous;
	
	
	/* NOME DO USUARIO */
	@NotNull
	@ManyToOne
	private CourseRealized courseRealized;
	
	
	@NotNull
	private StatusTestimony status;
	
	
	
	
	
	public StatusTestimony getStatus() {return status;}
	public void setStatus(StatusTestimony status) {this.status = status;}
	
	
	

	public Date getSend_date() {
		return send_date;
	}
	public void setSend_date(Date send_date) {
		this.send_date = send_date;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Boolean getAnonymous() {
		return anonymous;
	}
	public void setAnonymous(Boolean anonymous) {
		this.anonymous = anonymous;
	}
	
	
	
	public CourseRealized getCourseRealized() {
		return courseRealized;
	}
	public void setCourseRealized(CourseRealized courseRealized) {
		this.courseRealized = courseRealized;
	}
	@Override
	public int compareTo(Testimony  o) { return super.compareTo(o); }
	
	@Override
	public String toString() { return courseRealized.getCourse().getName() + " na data " + send_date.toString(); }
	
}
