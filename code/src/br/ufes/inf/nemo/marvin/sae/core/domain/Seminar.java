package br.ufes.inf.nemo.marvin.sae.core.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.jbutler.ejb.persistence.PersistentObjectSupport;




/**
 * CLASSE DE DOMMINIO QUE REPRESENTA OS SEMINARIOS.
 * 
 * IMPLEMENTADA COM BASE NO Documento de Análise de Requisitos VERSÃO 1.4
 * 
 * <i>ESTA CLASSE FAZ PARTE DO SISTEMA SAE.</i>
 * 
 * @author BRUNO MANZOLI (manzoli2122@gmail.com)
 */

@Entity
public class Seminar  extends PersistentObjectSupport implements Comparable<Seminar>{

	
	private static final long serialVersionUID = 1L;
	
	
	/** PALESTRANTE DO SEMINARIO */
	@Size(max = 60)
	private String panelist;
	
	
	/** DATA DO SEMINARIO */
	@Temporal(TemporalType.TIMESTAMP)
	private Date date_time;
	
	
	/** LOCAL ONDE SERÁ REALIZADO O SEMINARIO */
	@Size(max = 60)
	private String place;

	
	/** TITULO DO SEMINARIO */
	@NotNull
	@Size(max = 60)
	private String title;
	
	
	/** ASSUNTO DO SEMINARIO*/
	@NotNull
	@ManyToOne
	private TopicOfInterest topicOfInterest;
	
	
	@ManyToOne
	private Academic alumni_panelist;
	
	
	private boolean confirmada;
	
	
	
	
	
	

	@Override
	public int compareTo(Seminar  o) { return super.compareTo(o); }
	
	@Override
	public String toString() { return title; }

	
	
	/**  GETS AND SETS  */
	
	public String getPanelist() {
		return panelist;
	}
	public void setPanelist(String panelist) {
		if(alumni_panelist==null){
			this.panelist = panelist;
		}else
			this.panelist = alumni_panelist.getName();
	}

	public Date getDate_time() {
		return date_time;
	}
	public void setDate_time(Date date_time) {
		this.date_time = date_time;
	}

	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public TopicOfInterest getTopicOfInterest() {
		return topicOfInterest;
	}
	public void setTopicOfInterest(TopicOfInterest topicOfInterest) {
		this.topicOfInterest = topicOfInterest;
	}

	public Academic getAlumni_panelist() {
		return alumni_panelist;
	}
	public void setAlumni_panelist(Academic alumni_panelist) {
		this.alumni_panelist = alumni_panelist;
		if(alumni_panelist!=null)
			this.panelist = alumni_panelist.getName();
	}

	public boolean isConfirmada() {
		return confirmada;
	}

	public void setConfirmada(boolean confirmada) {
		this.confirmada = confirmada;
	}

	
}
