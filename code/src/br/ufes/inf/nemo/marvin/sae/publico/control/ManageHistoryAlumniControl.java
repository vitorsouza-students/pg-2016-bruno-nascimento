package br.ufes.inf.nemo.marvin.sae.publico.control;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;

import br.ufes.inf.nemo.marvin.core.application.SessionInformation;
import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.marvin.sae.core.domain.TopicOfInterest;
import br.ufes.inf.nemo.marvin.sae.core.persistence.TopicOfInterestDAO;
import br.ufes.inf.nemo.marvin.sae.publico.application.ManageHistoryAlumniService;
import br.ufes.inf.nemo.marvin.sae.publico.domain.HistoryAlumni;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudService;
import br.ufes.inf.nemo.jbutler.ejb.controller.CrudController;



/**
 * Controller class responsible for mediating the communication between user interface and application service for the
 * use case "Manage Historico".
 * 
 * This use case is a CRUD and, thus, the controller also uses the mini CRUD framework for EJB3.
 * 
 * @author Bruno Manzoli (manzoli2122@gmail.com)
 */
@Named
@SessionScoped
public class ManageHistoryAlumniControl  extends CrudController<HistoryAlumni>{

	
	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	/** The DAO for Assunto_Interesse objects. */
	@EJB
	private TopicOfInterestDAO topicOfInterestDAO;
	
	/** Information on the current visitor. */
	@EJB
	private SessionInformation sessionInformation;
	
	/** The logger. */
	private static final Logger logger = Logger.getLogger(ManageHistoryAlumniControl.class.getCanonicalName());
	
	
	/** The "Manage Historico" service. */
	@EJB
	private ManageHistoryAlumniService  manageHistoricoService ;
	
	
	private List<TopicOfInterest> listarAssuntos;
	
	private List<TopicOfInterest> myAssuntos;
	
	
	public List<TopicOfInterest> getListarAssuntos(){
		return listarAssuntos ;
	}
	
	public void listarAssuntos(){
		listarAssuntos = topicOfInterestDAO.retrieveAll();
		
		myAssuntos = new ArrayList<TopicOfInterest>();
		Academic usuario = sessionInformation.getCurrentUser();
		
		Iterator<TopicOfInterest> iterator = listarAssuntos.iterator();
		while(iterator.hasNext()){
			TopicOfInterest top = iterator.next();
			if(top.getAcademics().contains(usuario)){
				myAssuntos.add(top);
			}	
		}
	}
	
	
	
	
	/**   CONSTRUTOR DA CLASSE */
	public ManageHistoryAlumniControl(){
		 viewPath = "/sae/public/alumni/manageHistoryAlumni/";
	     bundleName = "msgsSAE";
	}
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#getCrudService() */
	@Override
	protected CrudService<HistoryAlumni> getCrudService() {
		manageHistoricoService.authorize();
		return manageHistoricoService;
	}

	

	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#initFilters() */
	@Override
	protected void initFilters() {	
	}
	
	
	
	@Override
	protected void retrieveEntities() {		
		if (lastEntityIndex > entityCount) lastEntityIndex = (int) entityCount;
		entities = manageHistoricoService.retrieveAllMine();
		lastEntityIndex = firstEntityIndex + entities.size();		
	}
	

	

	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#delete() */
	@Override
	public String delete() {
		try{
			return super.delete();
		}
		catch(Exception e){
			addGlobalI18nMessage(getBundleName(), FacesMessage.SEVERITY_ERROR, getBundlePrefix() + ".error.delete", summarizeSelectedEntity());
			cancelDeletion();
			return null;
		}
	}
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.controller.CrudController#save() */
	@Override
	public String save() {
		
		Academic usuario = sessionInformation.getCurrentUser();
		Iterator<TopicOfInterest> iterator = listarAssuntos.iterator();
		
		while(iterator.hasNext()){
			TopicOfInterest top = iterator.next();
			if(myAssuntos.contains(top)){
				if(!top.getAcademics().contains(usuario)){
					top.getAcademics().add(usuario);
					topicOfInterestDAO.save(top);
				}
				
			}
			else{
				if(top.getAcademics().contains(usuario)){
					top.getAcademics().remove(usuario);
					topicOfInterestDAO.save(top);
				}				
			}
			
		}
		
		try{
			return super.save();
		}
		catch(Exception e){
			logger.log(Level.INFO, "ERRO AO SALVAR    Historico_Egresso ......");
			addGlobalI18nMessage(getBundleName(), FacesMessage.SEVERITY_ERROR, getBundlePrefix() + ".error.save" , summarizeSelectedEntity()  );
			return null;
		}
	}
	
	
	@Override
	public String list() {
		listarAssuntos();
		return super.list();
	}
	
	public boolean isAlteravel(){
		return manageHistoricoService.isAlteravel(selectedEntity);
	}
	
	
	public List<TopicOfInterest> getMyAssuntos() {
		return myAssuntos;
	}
	public void setMyAssuntos(List<TopicOfInterest> myAssuntos) {
		this.myAssuntos = myAssuntos;
	}
	
	
}
