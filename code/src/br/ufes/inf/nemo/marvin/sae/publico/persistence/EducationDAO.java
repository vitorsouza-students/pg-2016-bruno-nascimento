package br.ufes.inf.nemo.marvin.sae.publico.persistence;

import java.util.List;

import javax.ejb.Local;

import br.ufes.inf.nemo.marvin.core.domain.Academic;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Education;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;

@Local
public interface EducationDAO extends BaseDAO<Education> {

	List<Education> retrieveAllMine(Academic academic);

}
