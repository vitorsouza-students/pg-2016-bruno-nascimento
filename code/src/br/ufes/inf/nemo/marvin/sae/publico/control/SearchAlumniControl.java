package br.ufes.inf.nemo.marvin.sae.publico.control;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.marvin.sae.core.domain.CourseRealized;
import br.ufes.inf.nemo.marvin.sae.publico.application.SearchAlumniService;


@Named
@SessionScoped
public class SearchAlumniControl implements Serializable{

private static final long serialVersionUID = 1L;
	
	private Course course;
	
	protected String viewPath;
	
	protected String bundleName;
	
	@EJB 
	private SearchAlumniService searchAlumniService;
	
	
	/*   CONSTRUTOR DA CLASSE */
	public SearchAlumniControl(){
		 viewPath = "/sae/public/search/alumni/";
	     bundleName = "msgsSAE";
	}
	
	
	public boolean getFacesRedirect() { return true; }
	public String getViewPath() {return viewPath;	}
	
	
	
	
	
	public String alumni(){
		course = null;
		return getViewPath()  + "index.xhtml?faces-redirect=" + getFacesRedirect();
	}
	
	public String showAlumni(){
		
		return getViewPath()  + "alumni.xhtml?faces-redirect=" + getFacesRedirect();
	}
	
	
	
	public Course getCourse() { return course; }
	public void setCourse(Course course) { this.course = course; }
	
	
	
	public List<CourseRealized> getAlumni(){
		return searchAlumniService.getAlumnis(course);
	}
	
	
	
	
}
