package br.ufes.inf.nemo.marvin.sae.core.application;


import javax.annotation.security.DeclareRoles;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;

import br.ufes.inf.nemo.marvin.core.application.CoreInformation;
import br.ufes.inf.nemo.marvin.core.domain.MarvinConfiguration;
import br.ufes.inf.nemo.marvin.sae.core.domain.Seminar;
import br.ufes.inf.nemo.marvin.sae.core.persistence.SeminarDAO;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudException;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudServiceBean;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;




@Stateless
@DeclareRoles({ "Admin" , "Alumni" , "Researcher" , "Student" , "Teacher" })
@RolesAllowed({ "Admin" , "Teacher" })
public class ManageSeminarsServiceBean  extends CrudServiceBean<Seminar> implements ManageSeminarsService {

	private static final long serialVersionUID = 1L;
	
	@EJB
	private SeminarDAO seminarDAO;
	
	/** The information singleton for the core module. */
	@EJB    	
	private CoreInformation coreInformation;
	
	@Override
	public BaseDAO<Seminar> getDAO() {
		return seminarDAO;
	}
	
	@Override
	public void authorize() {
		super.authorize();
	}
	
	public void validateCreate(Seminar entity) throws CrudException {
		entity.setConfirmada(false);
	}
	
	
	@Override
	public void validateUpdate(Seminar entity) throws CrudException {
		if(entity.isConfirmada()){
			alterSeminar(entity);
		}
		super.validateUpdate(entity);
	}

public void alterSeminar(Seminar seminar){
		
		MultiPartEmail email;
		
		String emailAddress = "manzoli2122@gmail.com";
		MarvinConfiguration config = coreInformation.getCurrentConfig(); 
		
		String msg = "Estamos convidando você a participar do seminário que sofreu algumas alterações:\n"
					+ " Assunto: " + seminar.getTopicOfInterest().getName()
					+ ";\n Titulo: " +seminar.getTitle()
					+ ";\n Data: " + seminar.getDate_time().toString()
					+ ";\n Local: " + seminar.getPlace()
					+ ";\n Palestrante " + seminar.getPanelist() +";";
		
		try{
			 email = new MultiPartEmail();
			 email.setHostName(config.getSmtpServerAddress());
			 email.setSmtpPort(config.getSmtpServerPort());
			 email.setAuthenticator(new DefaultAuthenticator(config.getSmtpUsername(), config.getSmtpPassword()));
			 email.setTLS(true);
			 email.setFrom(config.getSmtpUsername(), "Marvin");
			 
			 email.setSubject("Alteração em Seminário");
			 email.setMsg(msg);
			 email.addTo(emailAddress);
			 email.addTo("manzoli.elisandra@gmail.com");
			 email.addTo(config.getSmtpUsername());
			 email.send();
			 
		
		}
		catch (EmailException e) {
			e.printStackTrace();
		}	
		
	}
	
	
	
	@Override
	public void confirmSeminar(Seminar seminar){
		
		MultiPartEmail email;
		
		String emailAddress = "manzoli2122@gmail.com";
		MarvinConfiguration config = coreInformation.getCurrentConfig(); 
		
		String msg = "Estamos convidando você a participar do seminário:\n"
					+ " Assunto: " + seminar.getTopicOfInterest().getName()
					+ ";\n Titulo: " +seminar.getTitle()
					+ ";\n Data: " + seminar.getDate_time().toString()
					+ ";\n Local: " + seminar.getPlace()
					+ ";\n Palestrante " + seminar.getPanelist() +";";
		
		try{
			 email = new MultiPartEmail();
			 email.setHostName(config.getSmtpServerAddress());
			 email.setSmtpPort(config.getSmtpServerPort());
			 email.setAuthenticator(new DefaultAuthenticator(config.getSmtpUsername(), config.getSmtpPassword()));
			 email.setTLS(true);
			 email.setFrom(config.getSmtpUsername(), "Marvin");
			 
			 email.setSubject("Convite Seminário");
			 email.setMsg(msg);
			 email.addTo(emailAddress);
			 email.addTo("manzoli.elisandra@gmail.com");
			 email.addTo(config.getSmtpUsername());
			 email.send();
			 
		
		}
		catch (EmailException e) {
			e.printStackTrace();
		}	
		
		seminar.setConfirmada(true);
		seminarDAO.save(seminar);
	}
	
	
	@Override
	public void invitePanelist(Seminar seminar){
		
	}
	
	
	@Override
	@PermitAll
	public void volunteerPanelist(Seminar seminar){
		
	}

}
