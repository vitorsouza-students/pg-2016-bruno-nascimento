package br.ufes.inf.nemo.marvin.sae.core.application;

import java.util.List;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.ufes.inf.nemo.marvin.core.application.SessionInformation;
import br.ufes.inf.nemo.marvin.sae.core.domain.CourseRealized;
import br.ufes.inf.nemo.marvin.sae.core.persistence.CourseRealizedDAO;
import br.ufes.inf.nemo.jbutler.ejb.application.CrudServiceBean;
import br.ufes.inf.nemo.jbutler.ejb.persistence.BaseDAO;


/**
 * Stateless session bean implementing the "Manage CursoRealizado" use case component. See the implemented interface
 * documentation for details.
 * 
 * @author Bruno Manzoli (manzoli2122@gmail.com)
 * @see sae.core.application.ManageAdministradorService
 */
@Stateless
@DeclareRoles({ "Admin" , "Alumni" , "Researcher" , "Student" , "Teacher" })
@RolesAllowed({ "Admin" , "Teacher" })
public class ManageCoursesRealizedServiceBean extends CrudServiceBean<CourseRealized> implements ManageCoursesRealizedService{

	
	/** Serialization id. */
	private static final long serialVersionUID = 1L;
	
	
	/** The DAO for CursoRealizado objects. */
	@EJB    	
	private CourseRealizedDAO courseRealizedDAO;
	
	@EJB
	private SessionInformation sessionInformation;
	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.application.CrudService#getDAO() */
	@Override
	public BaseDAO<CourseRealized> getDAO() {
		return courseRealizedDAO;
	}

	
	
	
	/** @see br.ufes.inf.nemo.util.ejb3.application.CrudService#authorize() */
	@Override
	public void authorize() {
		super.authorize();
	}
	
	
	

	@PermitAll
	@Override
	public List<CourseRealized> getMyCourseRealized() {
		return courseRealizedDAO.retrieveMyCourses(sessionInformation.getCurrentUser());
	}

}
