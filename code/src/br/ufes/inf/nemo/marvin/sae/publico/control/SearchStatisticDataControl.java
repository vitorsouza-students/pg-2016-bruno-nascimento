package br.ufes.inf.nemo.marvin.sae.publico.control;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import org.primefaces.model.chart.PieChartModel;

import br.ufes.inf.nemo.marvin.core.domain.Course;
import br.ufes.inf.nemo.marvin.people.domain.Gender;
import br.ufes.inf.nemo.marvin.sae.publico.application.SearchStatisticDataService;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Acting_Area;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Degree_Education;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Formation_Area;
import br.ufes.inf.nemo.marvin.sae.publico.domain.Salary_Level;
import br.ufes.inf.nemo.marvin.sae.publico.domain.HistoryAlumni;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

@Named
@SessionScoped
public class SearchStatisticDataControl implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	//private static final Logger logger = Logger.getLogger(ConsultaDadosEstatisticosControl.class.getCanonicalName());
	
	@EJB 
	private SearchStatisticDataService searchStatisticDataService;

	protected String viewPath;
	
	protected String bundleName;
	
	private Course course;
	
	private PieChartModel  graphic_degreeEducation, graphic_gender, graphic_acting_area,  graphic_residential, graphic_acting_area_residential   ;
	
	private PieChartModel  graphic_formation_area, graphic_formation_area_residential;
	
	private PieChartModel graphic_salaryLevel, graphic_salaryLevel_businessman , graphic_salaryLevel_teacher, graphic_salaryLevel_residential;
	
	private List<HistoryAlumni>  Histories;
 
	private int number_acting_area_residential, number_salaryLevel_residential, number_salaryLevel_businessman , number_salaryLevel_teacher, number_formation_area_residential;
	
	
	
	/*   CONSTRUTOR DA CLASSE */
	public SearchStatisticDataControl(){
		 viewPath = "/sae/public/search/statisticData/";
	     bundleName = "msgsSAE";
	}
	
	
	
	public boolean getFacesRedirect() { return true; }
	public String getViewPath() {return viewPath;	}
	
	
	
	public String search() {
		course = null;
		return getViewPath() + "course.xhtml?faces-redirect=" + getFacesRedirect();
	}
	

	
	
	public String selectGraphic(){
		Histories = searchStatisticDataService.searchHistories(course);
		return getViewPath() + "graphic.xhtml?faces-redirect=" + getFacesRedirect();
	}
	
	
	
	
	
	
	
	
	
	
	
	// FUNCÕES DE FAIXA SALARIAL
	public String search_salaryLevel_Residential() {
			
		Iterator<HistoryAlumni> iterator = Histories.iterator();
		Salary_Level[] faixas = Salary_Level.values();
		
		
		int[] valor = new int[faixas.length] ;
		int[] valor_empreendedor = new int[faixas.length] ;
		int[] valor_professor = new int[faixas.length] ;
		
		
		graphic_salaryLevel_residential = new PieChartModel();
		graphic_salaryLevel_businessman = new PieChartModel();
		graphic_salaryLevel_teacher = new PieChartModel();
		
		
		number_salaryLevel_residential=0;
		number_salaryLevel_businessman=0;
		number_salaryLevel_teacher=0;
		
		
		while(iterator.hasNext()){
			
			HistoryAlumni hh = iterator.next();
			Salary_Level ff = hh.getSalary_Level();
			
			if(hh.getActing_Area().equals(Acting_Area.PROFESSOR)){
				for(int i = 0 ; i < faixas.length ; i++ ){
					if(  ff.equals(faixas[i])  ){
						valor_professor[i]++;
						number_salaryLevel_teacher++;
						break;
					}
				}
			}
			
			
			if(hh.getActing_Area().equals(Acting_Area.EMPREENDEDOR)){
				for(int i = 0 ; i < faixas.length ; i++ ){
					if(  ff.equals(faixas[i])  ){
						valor_empreendedor[i]++;
						number_salaryLevel_businessman++;
						break;
					}
				}
			}
			
			
			if(hh.getLives_in_ES()){
				for(int i = 0 ; i < faixas.length ; i++ ){
					if(  ff.equals(faixas[i])  ){
						valor[i]++;
						number_salaryLevel_residential++;
						break;
					}
				}
			}	
			
		}
		
		for(int i = 0 ; i < faixas.length ; i++ ){
			graphic_salaryLevel_residential.set(faixas[i].getLabel(),valor[i]);
			graphic_salaryLevel_businessman.set(faixas[i].getLabel(),valor_empreendedor[i]);
			graphic_salaryLevel_teacher.set(faixas[i].getLabel(),valor_professor[i]);
		}
		
		
		graphic_salaryLevel_teacher.setTitle("Egressos Professores");
		graphic_salaryLevel_teacher.setLegendPosition("s");
		graphic_salaryLevel_teacher.setShowDataLabels(true);
		graphic_salaryLevel_teacher.setSeriesColors("dd6542,E7982F,E4F20A,0AC9F2,7a86db,6a9660");
	    graphic_salaryLevel_teacher.setDiameter(250);
		
		
		graphic_salaryLevel_businessman.setTitle("Egressos Empreendedores");
		graphic_salaryLevel_businessman.setLegendPosition("s");
		graphic_salaryLevel_businessman.setShowDataLabels(true);
		graphic_salaryLevel_businessman.setSeriesColors("dd6542,E7982F,E4F20A,0AC9F2,7a86db,6a9660");
	    graphic_salaryLevel_businessman.setDiameter(250);
		
		
		graphic_salaryLevel_residential.setTitle("Egressos Residente no Espirito Santo");
		graphic_salaryLevel_residential.setLegendPosition("s");
		graphic_salaryLevel_residential.setShowDataLabels(true);
		graphic_salaryLevel_residential.setSeriesColors("dd6542,E7982F,E4F20A,0AC9F2,7a86db,6a9660");
	    //faixaSalarial.setDataFormat("value percent");
		graphic_salaryLevel_residential.setDiameter(250);
			
		return getViewPath() + "graphicSalaryLevel.xhtml?faces-redirect=" + getFacesRedirect();
	}
		
	public PieChartModel getgraphic_salaryLevel_residential() {  return graphic_salaryLevel_residential;   }
	public PieChartModel getGraphic_salaryLevel_businessman() {  return graphic_salaryLevel_businessman;   }
	public PieChartModel getGraphic_salaryLevel_teacher() {  return graphic_salaryLevel_teacher;   }
		
		
	
	
	
	
	
	
	// FUNCÕES DE FAIXA SALARIAL
	
	public String search_Salary_Level() {
		
		graphic_salaryLevel = new PieChartModel();
		Iterator<HistoryAlumni> iterator = Histories.iterator();
		Salary_Level[] faixas = Salary_Level.values();
		int[] valor = new int[faixas.length] ;
		
		while(iterator.hasNext()){
			Salary_Level ff = iterator.next().getSalary_Level();
			for(int i = 0 ; i < faixas.length ; i++ ){
				if(  ff.equals(faixas[i])  ){
					valor[i]++;
					break;
				}
			}
		}
		for(int i = 0 ; i < faixas.length ; i++ ){
			graphic_salaryLevel.set(faixas[i].getLabel(),valor[i]);
		}
		graphic_salaryLevel.setTitle("Todos os Egressos");
		graphic_salaryLevel.setLegendPosition("s");
		graphic_salaryLevel.setShowDataLabels(true);
        //faixaSalarial.setDataFormat("value percent");
		graphic_salaryLevel.setDiameter(250);
		//faixaSalarial.setSeriesColors("c95939,E7982F,E4F20A,0AC9F2,2C0AF2,178504");
		graphic_salaryLevel.setSeriesColors("dd6542,E7982F,E4F20A,0AC9F2,7a86db,6a9660");
		
		search_salaryLevel_Residential();
		return getViewPath() + "graphicSalaryLevel.xhtml?faces-redirect=" + getFacesRedirect();
	}
	
	public PieChartModel getGraphic_salaryLevel() {  return graphic_salaryLevel;   }
	
	
	
	
	
	
	
	
	
	
	
	
	// FUNCÕES DE graphic_residential
	
	public String search_residential() {
		
		graphic_residential = new PieChartModel();
		Iterator<HistoryAlumni> iterator = Histories.iterator();
		int reside = 0, naoreside = 0;
		while(iterator.hasNext()){
			if(  iterator.next().getLives_in_ES()  ){
				reside++;
			}
			else {
				naoreside++;
			}
		}
		graphic_residential.set("Reside no ES",reside);
		graphic_residential.set("Não Reside no ES",naoreside);
		
		graphic_residential.setTitle("Reside no Espirito Santo");
		graphic_residential.setLegendPosition("s");
		graphic_residential.setShowDataLabels(true);
		graphic_residential.setDiameter(250);
		
		return getViewPath() + "graphicResidential.xhtml?faces-redirect=" + getFacesRedirect();
	}
	
	public PieChartModel getGraphic_residential() {  return graphic_residential;   }
	 
	 
	 
	 
	 
	
	
	public String search_Acting_Area() {
		
		graphic_acting_area = new PieChartModel();
		Iterator<HistoryAlumni> iterator = Histories.iterator();
		
		Acting_Area[] areas = Acting_Area.values();
		
		int[] valor = new int[areas.length] ;
		
		while(iterator.hasNext()){
			Acting_Area aa = iterator.next().getActing_Area();
			for(int i = 0 ; i < areas.length ; i++ ){
				if(  aa.equals(areas[i])  ){
					valor[i]++;
					break;
				}
			}
		}
		for(int i = 0 ; i < areas.length ; i++ ){
			graphic_acting_area.set(areas[i].getLabel(),valor[i]);
		}
		graphic_acting_area.setTitle("Todos os Egressos");
		graphic_acting_area.setLegendPosition("s");
		graphic_acting_area.setShowDataLabels(true);
		graphic_acting_area.setDiameter(250);
        //areaAtuacao.setSeriesColors("dd6542,E7982F,E4F20A,0AC9F2,7a86db,6a9660");
		graphic_acting_area.setSeriesColors("dd6542,E7982F,0AC9F2,7a86db,6a9660");
		
		search_Acting_Area_Residential();
		return getViewPath() + "graphicActingArea.xhtml?faces-redirect=" + getFacesRedirect();
	}
	
	public PieChartModel getGraphic_acting_area() {  return graphic_acting_area;   }
	
	
	
	
	// FUNCÕES DE Formation_Area
	public void search_Acting_Area_Residential() {
					
				Iterator<HistoryAlumni> iterator = Histories.iterator();
				Acting_Area[] areas = Acting_Area.values();
				int[] valor = new int[areas.length] ;
				
				graphic_acting_area_residential = new PieChartModel();
				
				number_acting_area_residential=0;
				
				
				while(iterator.hasNext()){
					
					HistoryAlumni hh = iterator.next();
					Acting_Area ff = hh.getActing_Area();
					
					if(hh.getLives_in_ES()){
						for(int i = 0 ; i < areas.length ; i++ ){
							if(  ff.equals(areas[i])  ){
								valor[i]++;
								number_acting_area_residential++;
								break;
							}
						}
					}	
					
				}
				
				for(int i = 0 ; i < areas.length ; i++ ){
					graphic_acting_area_residential.set(areas[i].getLabel(),valor[i]);
					
				}
				
				
				graphic_acting_area_residential.setTitle("Egressos Residente no Espirito Santo");
				graphic_acting_area_residential.setLegendPosition("s");
				graphic_acting_area_residential.setShowDataLabels(true);
				graphic_acting_area_residential.setSeriesColors("dd6542,E7982F,0AC9F2,7a86db,6a9660");
				graphic_acting_area_residential.setDiameter(250);
				
			}
				
			public PieChartModel getgraphic_acting_area_residential() {  return graphic_acting_area_residential;   }
			
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public String search_Gender() {
		
		graphic_gender = new PieChartModel();
		Iterator<HistoryAlumni> iterator = Histories.iterator();
		
		int feminino = 0 ;
		
		while(iterator.hasNext()){
			Gender ss = iterator.next().getAlumni().getGender();
			if(ss!=null)
				if(  ss.equals(Gender.Female)  ){
					feminino++;
				}
			
		}
		
		graphic_gender.set("Feminino", feminino);
		graphic_gender.set("Masculino", (Histories.size()-feminino));
		
		
		graphic_gender.setTitle("Todos os Egressos");
		graphic_gender.setLegendPosition("s");
		graphic_gender.setShowDataLabels(true);
		graphic_gender.setDiameter(250);
        graphic_gender.setSeriesColors("dd6542,7a86db");
		
		return getViewPath() + "graphicGender.xhtml?faces-redirect=" + getFacesRedirect();
	}
	
	public PieChartModel getGraphic_gender() {  return graphic_gender;   }
	
	
	 
	
	
	
	
	
	
	public String search_Formation_Area() {
		graphic_formation_area = new PieChartModel();
		Iterator<HistoryAlumni> iterator = Histories.iterator();
		Formation_Area[] areas = Formation_Area.values();
		int[] valor = new int[areas.length] ;
		while(iterator.hasNext()){
			Formation_Area aa = iterator.next().getOperates_in_area_formation();
			for(int i = 0 ; i < areas.length ; i++ ){
				if(  aa.equals(areas[i])  ){
					valor[i]++;
					break;
				}
			}
		}
		for(int i = 0 ; i < areas.length ; i++ ){
			graphic_formation_area.set(areas[i].getLabel(),valor[i]);
		}
		graphic_formation_area.setTitle("Todos os Egressos");
		graphic_formation_area.setLegendPosition("s");
		graphic_formation_area.setShowDataLabels(true);
		graphic_formation_area.setDiameter(250);
        //areaAtuacao.setSeriesColors("dd6542,E7982F,E4F20A,0AC9F2,7a86db,6a9660");
		graphic_formation_area.setSeriesColors("6a9660,0AC9F2,dd6542");
		search_Formation_Area_Residential();
		return getViewPath() + "graphicFormationArea.xhtml?faces-redirect=" + getFacesRedirect();
	}
	
	public PieChartModel getGraphic_formation_area() {  return graphic_formation_area;   }
	
	
	
	
	
	
	// FUNCÕES DE Formation_Area
		public String search_Formation_Area_Residential() {
				
			Iterator<HistoryAlumni> iterator = Histories.iterator();
			
			Formation_Area[] areas = Formation_Area.values();
			
			int[] valor = new int[areas.length] ;
			//int[] valor_empreendedor = new int[areas.length] ;
			//int[] valor_professor = new int[areas.length] ;
			
			
			graphic_formation_area_residential = new PieChartModel();
			//graphic_salaryLevel_businessman = new PieChartModel();
			//graphic_salaryLevel_teacher = new PieChartModel();
			
			
			number_formation_area_residential=0;
			//number_salaryLevel_businessman=0;
			//number_salaryLevel_teacher=0;
			
			
			while(iterator.hasNext()){
				
				HistoryAlumni hh = iterator.next();
				Formation_Area ff = hh.getOperates_in_area_formation();
				
				/*
				if(hh.getActing_Area().equals(Acting_Area.PROFESSOR)){
					for(int i = 0 ; i < faixas.length ; i++ ){
						if(  ff.equals(faixas[i])  ){
							valor_professor[i]++;
							number_salaryLevel_teacher++;
							break;
						}
					}
				}
				
				
				if(hh.getActing_Area().equals(Acting_Area.EMPREENDEDOR)){
					for(int i = 0 ; i < faixas.length ; i++ ){
						if(  ff.equals(faixas[i])  ){
							valor_empreendedor[i]++;
							number_salaryLevel_businessman++;
							break;
						}
					}
				}
				*/
				
				if(hh.getLives_in_ES()){
					for(int i = 0 ; i < areas.length ; i++ ){
						if(  ff.equals(areas[i])  ){
							valor[i]++;
							number_formation_area_residential++;
							break;
						}
					}
				}	
				
			}
			
			for(int i = 0 ; i < areas.length ; i++ ){
				graphic_formation_area_residential.set(areas[i].getLabel(),valor[i]);
				//graphic_salaryLevel_businessman.set(faixas[i].getLabel(),valor_empreendedor[i]);
				//graphic_salaryLevel_teacher.set(faixas[i].getLabel(),valor_professor[i]);
			}
			
			/*
			graphic_salaryLevel_teacher.setTitle("Egressos Professores");
			graphic_salaryLevel_teacher.setLegendPosition("s");
			graphic_salaryLevel_teacher.setShowDataLabels(true);
			graphic_salaryLevel_teacher.setSeriesColors("dd6542,E7982F,E4F20A,0AC9F2,7a86db,6a9660");
		    graphic_salaryLevel_teacher.setDiameter(250);
			
			
			graphic_salaryLevel_businessman.setTitle("Egressos Empreendedores");
			graphic_salaryLevel_businessman.setLegendPosition("s");
			graphic_salaryLevel_businessman.setShowDataLabels(true);
			graphic_salaryLevel_businessman.setSeriesColors("dd6542,E7982F,E4F20A,0AC9F2,7a86db,6a9660");
		    graphic_salaryLevel_businessman.setDiameter(250);
			
			*/
			
			graphic_formation_area_residential.setTitle("Egressos Residente no Espirito Santo");
			graphic_formation_area_residential.setLegendPosition("s");
			graphic_formation_area_residential.setShowDataLabels(true);
			graphic_formation_area_residential.setSeriesColors("6a9660,0AC9F2,dd6542");
		    //faixaSalarial.setDataFormat("value percent");
			graphic_formation_area_residential.setDiameter(250);
				
			return getViewPath() + "graphicFormationArea.xhtml?faces-redirect=" + getFacesRedirect();
		}
			
		public PieChartModel getgraphic_formation_area_residential() {  return graphic_formation_area_residential;   }
		//public PieChartModel getGraphic_salaryLevel_businessman() {  return graphic_salaryLevel_businessman;   }
		//public PieChartModel getGraphic_salaryLevel_teacher() {  return graphic_salaryLevel_teacher;   }
			
		
		
		
		
		
		
		
		
		
		// FUNCÕES DE degreeEducation
		
		public String search_Degree_Education() {
			
			graphic_degreeEducation = new PieChartModel();
			Iterator<HistoryAlumni> iterator = Histories.iterator();
			
			Degree_Education[] faixas = Degree_Education.values();
			int[] valor = new int[faixas.length] ;
			
			while(iterator.hasNext()){
				Degree_Education ff = iterator.next().getDegree_Education();
				for(int i = 0 ; i < faixas.length ; i++ ){
					if(  ff.equals(faixas[i])  ){
						valor[i]++;
						break;
					}
				}
			}
			for(int i = 0 ; i < faixas.length ; i++ ){
				graphic_degreeEducation.set(faixas[i].getLabel(),valor[i]);
			}
			graphic_degreeEducation.setTitle("Todos os Egressos");
			graphic_degreeEducation.setLegendPosition("s");
			graphic_degreeEducation.setShowDataLabels(true);
	        //faixaSalarial.setDataFormat("value percent");
			graphic_degreeEducation.setDiameter(250);
			//faixaSalarial.setSeriesColors("c95939,E7982F,E4F20A,0AC9F2,2C0AF2,178504");
			graphic_degreeEducation.setSeriesColors("E7982F,E4F20A,0AC9F2,7a86db,6a9660");
			
			return getViewPath() + "graphicDegreeEducation.xhtml?faces-redirect=" + getFacesRedirect();
		}
		
		public PieChartModel getGraphic_degreeEducation() {  return graphic_degreeEducation;   }
		
		
	
	 
	
	

	public Course getCourse() { return course; }
	public void setCourse(Course course) { this.course = course; }
	
	public int getNumberAlumni() { return Histories.size(); }

	public int getNumber_salaryLevel_residential() { return number_salaryLevel_residential; }

	public int getNumber_salaryLevel_businessman() { return number_salaryLevel_businessman; }
	
	public int getNumber_salaryLevel_teacher() { return number_salaryLevel_teacher; }

	public int getNumber_formation_area_residential() {	return number_formation_area_residential;}

	public int getNumber_acting_area_residential() { return number_acting_area_residential; 	}





}
